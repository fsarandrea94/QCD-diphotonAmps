(* ::Package:: *)

(* ::Subsubsection:: *)
(*Trying to work on the bubble coefficients of 3g2a ( --+++)*)


Quit[]


<<FiniteFlow`


SetDirectory["/home/fsarandrea/git/gitlab/myfiniteflowexamples"];


exSubs=Rule@@@Transpose[{ex[#]&/@Range[5], RandomInteger[10^4,5]}];


<<"InitTwoLoopToolsFF.m";
process = "gggaa";
psmode = "PSanalytic";
<<"amplitudes/GlobalMapProcessSetup.m";


(* ::Item:: *)
(*A series of useful functions to guess the degree of a sliced coefficient and match it to  known factors*)


GetDegree[expr_]:=deg[Exponent[Numerator[expr],t],Exponent[Denominator[expr],t]]; 
GetDegreeMax[expr_List] := Module[{degs,maxnum,maxden},
  degs = GetDegree/@expr;
  maxnum = Max@degs[[;;,1]];
  maxden = Max@degs[[;;,2]];
  Return[{maxnum,maxden}];
];

fixDeltaSign[expr_, term_]:=Module[{selected},
selected=Select[Select[FactorList[expr][[All,1]], Variables[#]!={}&],((#/.\[Delta]->0 /.exSubs)==(term/.exSubs))&]; 
If[selected=={},
Return[{0}];
,
Return[selected[[1]]];
]; ];

outerPlus[l__]:=Outer[Plus,l];


Options[MatchCoefficientFactors]={"PrimeNo"->0};
MatchCoefficientFactors[slicedcoeffs_List, slicedfactorguess_List,OptionsPattern[]]:=Module[
{allfactors,factorguessclist,factorguessass,slicedcoeffsclist,slicedcoeffsclistass,
   eqs,sol,lhs,cs,rhss},
  
  factorguessclist = DeleteCases[FactorList[#], a_/;Variables[a]==={}]&/@slicedfactorguess[[All,2]];
  allfactors = Union[DeleteCases[Flatten[factorguessclist],a_/;Variables[a]==={}]];
  factorguessass = Table[Association[Rule@@#&/@cc],{cc,factorguessclist}];

  slicedcoeffsclist = Map[DeleteCases[FactorList[#], a_/;Variables[a]==={}]&,slicedcoeffs];
  slicedcoeffsclistass = Table[Association[(Rule@@#)&/@scf], {scf,slicedcoeffsclist}];

  cs = Array[tiz,Length[slicedfactorguess]];
  lhs = Table[cs.(#[fac]&/@factorguessass /. _Missing->0), {fac,allfactors}];
  rhss = Table[Table[(scfa[fac] /. _Missing->0), {fac,allfactors}],{scfa,slicedcoeffsclistass}];
  sol = Flatten[Quiet[Solve[lhs-#==0]]]&/@rhss;

  If[MemberQ[sol,{}], Print["No solution found"]; Return[{}],
    Return[Times@@(slicedfactorguess[[All,1]]^#) &/@(cs /. sol)] 
  ];
]


(* ::Item:: *)
(*Can project  poleansatz and coeffansatz into the \[Delta]->0 space*)


GetMomentumTwistorExpression[poleansatz, PSanalytic]/.ex[2]->(\[Delta]+ex[1] ex[4]+ex[1] ex[3] ex[4]-ex[1] ex[3] ex[5])/(ex[1] ex[3]) ;
polesDelta0=% /.\[Delta]->0 //Simplify;


GetMomentumTwistorExpression[coeffansatz, PSanalytic]/.ex[2]->(\[Delta]+ex[1] ex[4]+ex[1] ex[3] ex[4]-ex[1] ex[3] ex[5])/(ex[1] ex[3]) ;
coeffsDelta0=% /.\[Delta]->0 //Simplify;
coeffsDelta0=DeleteCases[coeffsDelta0,0];


(* ::Item:: *)
(*The 1/\[Delta]^2 can be obtained  by projecting Numerator and  Denominator/\[Delta]^2 onto the \[Delta]->0 space.*)


(* ::Item:: *)
(*This means that we can match the factor with polesDelta0 and coeffsDelta0*)


minus2Coeffs15delta=Coefficient[Normal@Series[(coeffLogs15 /.deltaSub), {\[Delta],0,-2}], 1/\[Delta]^2];
num0=Numerator[Together[coeffLogs15 /.deltaSub]] /.\[Delta]->0;
den0=Denominator[Together[coeffLogs15 /.deltaSub]]/\[Delta]^2 /.\[Delta]->0;
minus2Coeffs15delta*den0/num0 //Simplify


(* ::Item:: *)
(*Function which reconstruct the possible factors by starting with a pole*)


Options[extractFromProjection]={"DeeperPole"->0,"sliceRules"->{} };

extractFromProjection[expr1_, coeffansatz_, pole_, order_, OptionsPattern[]]:=Module[{residue,sub, expr, ansatzMT, listofPossibleFactors, zerothFactors, numFact,termsTomatch, 
                                                          exSubs, deltasLists, modFactorList,firstOrderInDelta,positionOfMatches, 
                                                          guessedTerms, posTerms,powPositions,pows,constFacts,powsConst,constTerm,
                                                          deltaTerms,couples,listofExprs, sign,res},

sub= (Solve[pole==\[Delta], ex[3]])[[1,1]];

expr=expr1-(OptionValue["DeeperPole"]/pole^(order+1) /. \[Delta]->pole);

residue= Coefficient[Normal@Series[(expr /.sub), {\[Delta],0,-order}], Power[\[Delta], -order]];
residue=residue /.OptionValue["sliceRules"];

ansatzMT=GetMomentumTwistorExpression[coeffansatz, PSanalytic] /.sub;

listofPossibleFactors=DeleteCases[Flatten[Table[(FactorList[#]&/@ansatzMT)[[i]][[All,1]], {i, Length@ansatzMT}]], a_ /;Variables[a]==={}] //DeleteDuplicates; 
zerothFactors=FactorList[residue];

numFact=Select[zerothFactors, Variables[#]=={}&][[1,1]];

termsTomatch= DeleteCases[zerothFactors[[All,1]], a_/;Variables[a]==={}];

exSubs=Rule@@@Transpose[{Variables[residue], RandomInteger[10^6,Length@Variables[residue]]}];

modFactorList=(Total[Abs[# /.Plus->List]] /.Abs[\[Delta]]->\[Delta])&/@(listofPossibleFactors /.OptionValue["sliceRules"] /. exSubs);
deltasLists=Table[((modFactorList)-Abs[(term/. exSubs)])/\[Delta], {term, termsTomatch}];

firstOrderInDelta=Table[(Select[deltasList, Variables[#]==={}&] //DeleteCases[#,0]& //DeleteDuplicates), {deltasList, deltasLists}];

positionOfMatches=Table[If[firstOrderInDelta[[i]]=={}, {}, Position[deltasLists[[i]],firstOrderInDelta[[i]][[1]]]] , {i, Length@deltasLists}];

guessedTerms=Table[If[posTerm=={}, {},listofPossibleFactors[[#]]&/@Flatten[posTerm]], {posTerm, positionOfMatches}];
guessedTerms= guessedTerms /.OptionValue["sliceRules"];

posTerms=(Table[If[term=={}, Nothing, Position[Abs[zerothFactors/.exSubs], Abs[(#/.\[Delta]->0 /.exSubs)]]&/@term ],{term, guessedTerms}])[[All,1,1]];
powPositions=Transpose[{posTerms, ConstantArray[2, Length@posTerms]}];

pows=(zerothFactors[[#[[1]],#[[2]]]]&/@powPositions)[[All,1]];

deltaTerms=Flatten[Table[If[Length@el>1, el /.el->el[[1]], el], {el, guessedTerms}]]^pows;

constFacts=termsTomatch[[#]]&/@Flatten[Position[guessedTerms, {}]];

powsConst=(zerothFactors[[#]]&/@Table[Position[(zerothFactors /.exSubs), (const /.exSubs)], {const, constFacts}][[All,1,1]])[[All,2]];

constTerm=numFact*Times@@(constFacts^powsConst);

couples=Transpose[{(deltaTerms /.\[Delta]->0), deltaTerms}];

listofExprs=Flatten[constTerm*KroneckerProduct@@couples];

sign=Times@@Sign[((residue /.exSubs)/(Select[listofExprs /.exSubs, Variables[#]=={}&]))];
res=Delete[sign*listofExprs,1];
If[(OptionValue["DeeperPole"] /. \[Delta]->1 /.exSubs)!=0, res=Outer[List,{OptionValue["DeeperPole"]}, res][[1]]];

Return[res];
]


(* ::Item:: *)
(*Function coeffsFromPoles does the same as extractFromProjection but it takes the guessed factors as an input. This is faster to use in nested loops*)


Flatten[ConstantArray[#[[1]], Abs[#[[2]]]]&/@%]


Options[coeffsFromPoles]={"DeeperPole"->0,"sliceRules"->{}, "printDeeper"->False};

coeffsFromPoles[expr_, listofPossibleFactors_, pole_, order_, OptionsPattern[]]:=Module[{residue, residue2, deltaRes,termToFix, residueList,sub, resList,ansatzMT, zerothFactors, numFact,termsTomatch, 
                                                          exSubs, deltasLists, modFactorList,firstOrderInDelta,positionOfMatches, 
                                                          guessedTerms, posTerms,pows,constFacts,constTerm,
                                                          deltaTerms,couples,listofExprs, sign,opsList, deltaCoeffs, opsTerms,res},

sub= (Solve[pole==\[Delta], ex[3]])[[1,1]]/.OptionValue["sliceRules"];



residue= Coefficient[Normal@Series[(expr /.sub), {\[Delta],0,-order}], Power[\[Delta], -order]];
residue2=Coefficient[Series[(OptionValue["DeeperPole"]/pole^(order+1)/. \[Delta]->pole) /.sub, {\[Delta],0,-order}], Power[\[Delta], -order]];
residue=residue /.OptionValue["sliceRules"];
residue2=residue2 /.OptionValue["sliceRules"];
deltaRes=Together[residue-residue2];
If[ deltaRes==0, 
Return[{0}];
Quit[];
];
exSubs=Rule@@@Transpose[{Variables[expr], RandomInteger[10^6,Length@Variables[expr]]}];

If[(OptionValue["DeeperPole"]/.exSubs)==0,
termToFix={}; 
,
termToFix=Select[Complement[FactorList[deltaRes][[All,1]],FactorList[residue][[All,1]]], Variables[#]!={}&];
];

zerothFactors=FactorList[deltaRes];

numFact=Select[zerothFactors, Variables[#]=={}&][[1,1]];

termsTomatch=DeleteCases[Flatten[ConstantArray[#[[1]], Abs[#[[2]]]]&/@zerothFactors], a_/;Variables[a]==={}];

pows=Flatten[ConstantArray[Sign[#[[2]]], Abs[#[[2]]]]&/@DeleteCases[zerothFactors,a_/;Variables[a]==={}]];

If["sliceRules"!={}, exSubs={}];

modFactorList=(Total[Abs[# /.Plus->List]] /.Abs[\[Delta]]->\[Delta])&/@(listofPossibleFactors /.OptionValue["sliceRules"] /. exSubs);

deltasLists=Table[((modFactorList)-Abs[(term/. exSubs)])/\[Delta], {term, termsTomatch}];

firstOrderInDelta=Table[(Select[deltasList, Variables[#]==={}&] //DeleteCases[#,0]& //DeleteDuplicates), {deltasList, deltasLists}];

positionOfMatches=Table[If[firstOrderInDelta[[i]]=={}, {}, Position[deltasLists[[i]],firstOrderInDelta[[i]][[1]]]] , {i, Length@deltasLists}];

guessedTerms=Table[If[posTerm=={}, {},listofPossibleFactors[[#]]&/@Flatten[posTerm]], {posTerm, positionOfMatches}];

guessedTerms= guessedTerms /.OptionValue["sliceRules"];

posTerms=(Table[If[term=={}, Nothing, Position[Abs[zerothFactors/.exSubs], Abs[(#/.\[Delta]->0 /.exSubs)]]&/@term ],{term, guessedTerms}])[[All,1,1]];


deltaTerms=Flatten[Table[If[Length@el>1, el /.el->el[[1]], el], {el, guessedTerms}]];
deltaTerms=deltaTerms^(pows[[#]]&/@Table[Position[guessedTerms,term][[1,1]], {term, deltaTerms}]);

constFacts=(termsTomatch[[#]]^pows[[#]])&/@Flatten[Position[guessedTerms, {}]];

constTerm=numFact*Times@@(constFacts);

couples=Transpose[{(deltaTerms /.\[Delta]->0), deltaTerms}];


If[ Length[couples]==1,
listofExprs=constTerm*Flatten[couples];
,
listofExprs=DeleteDuplicates[Flatten[constTerm*KroneckerProduct@@couples]];
];
sign=Times@@Sign[((deltaRes /.exSubs)/(Select[listofExprs /.exSubs, Variables[#]=={}&]))];

res=Delete[sign*listofExprs,1];
res=Select[res, (GetDegreeMax[FactorList[# /.\[Delta]->pole /.sliceRules]]/.{a_List:>Max[a]})<=8&];

If[termToFix!={},
deltaCoeffs=(DeleteCases[Coefficient[#,\[Delta]]&/@(Flatten[#[[All,1]]&/@(FactorList[#]&/@res)] //DeleteDuplicates),0] //DeleteDuplicates);
deltaCoeffs=Flatten[MonomialList@deltaCoeffs]//DeleteDuplicates;

(*deltaCoeffs=(((DeleteCases[Coefficient[#,\[Delta]]&/@(Flatten[#[[All,1]]&/@(FactorList[#]&/@listofPossibleFactors)]),0])//MonomialList //Flatten) /.-a_\[RuleDelayed]a ) //DeleteDuplicates;*)
opsList=Flatten[outerPlus@@(\[Delta]*deltaCoeffs*Table[Range[-2,2], {i,Length@deltaCoeffs}])];
opsTerms=Flatten[Table[termToFix+cf, {cf,opsList}]]/termToFix[[1]];
res=DeleteDuplicates[Flatten[Outer[Times,res, opsTerms]]];
If[OptionValue["printDeeper"], res=Outer[List,{OptionValue["DeeperPole"]}, res][[1]]];
];

Return[res];
]


(* ::Item:: *)
(*List of all the log coefficients*)


sliceRules=Rule@@@Transpose[{ex[#]&/@Range[5], RandomInteger[10^4,5]+RandomInteger[10^4,5]*t}];


coeffLogs24=-((64 ex[1]^3 ex[2]^4 (1+ex[2]) ex[3]^2)/((1+ex[3]) (1+ex[2]+ex[3])))-(64 (1+ex[2]) (-ex[1]^3 ex[2]^6 ex[3]^3+ex[1]^3 ex[2]^6 ex[3]^3 ex[5]))/((1+ex[3])^2 (1+ex[2]+ex[3]) (-ex[2] ex[3]+ex[4]+ex[2] ex[4]+ex[3] ex[4]+ex[2] ex[3] ex[5]))-(64 (1+ex[2]) (3 ex[1]^3 ex[2]^4 ex[3]^4 ex[5]+5 ex[1]^3 ex[2]^4 ex[3]^5 ex[5]+3 ex[1]^3 ex[2]^5 ex[3]^5 ex[5]+2 ex[1]^3 ex[2]^4 ex[3]^6 ex[5]+2 ex[1]^3 ex[2]^5 ex[3]^6 ex[5]))/((1+ex[3])^2 (-ex[2]-ex[2] ex[3]-ex[2]^2 ex[3]+ex[4]+ex[3] ex[4]+ex[2] ex[3] ex[4]+ex[2] ex[5]))-(64 (ex[1]^3 ex[2]^5 ex[3]^5 ex[5]^2+2 ex[1]^3 ex[2]^6 ex[3]^5 ex[5]^2+ex[1]^3 ex[2]^7 ex[3]^5 ex[5]^2))/((1+ex[3]) (-ex[2]-ex[2] ex[3]-ex[2]^2 ex[3]+ex[4]+ex[3] ex[4]+ex[2] ex[3] ex[4]+ex[2] ex[5])^2);
coeffLogs14=-((64 ex[1]^3 ex[2]^5 ex[3]^2)/((-1+ex[2]-ex[3]) (1+ex[3])))-(64 (-ex[1]^3 ex[2]^6 ex[3]^2-ex[1]^3 ex[2]^6 ex[3]^3-ex[1]^3 ex[2]^7 ex[3]^3+ex[1]^3 ex[2]^6 ex[3]^2 ex[5]+ex[1]^3 ex[2]^6 ex[3]^3 ex[5]+ex[1]^3 ex[2]^7 ex[3]^3 ex[5]))/((-1+ex[2]-ex[3]) (1+ex[3])^2 (ex[2]+ex[2] ex[3]-ex[4]+ex[2] ex[4]-ex[3] ex[4]-ex[2] ex[5]-ex[2] ex[3] ex[5]))-(64 (2 ex[1]^3 ex[2]^2 ex[3]^3 ex[5]+3 ex[1]^3 ex[2]^3 ex[3]^3 ex[5]+6 ex[1]^3 ex[2]^2 ex[3]^4 ex[5]+12 ex[1]^3 ex[2]^3 ex[3]^4 ex[5]+6 ex[1]^3 ex[2]^4 ex[3]^4 ex[5]+6 ex[1]^3 ex[2]^2 ex[3]^5 ex[5]+15 ex[1]^3 ex[2]^3 ex[3]^5 ex[5]+12 ex[1]^3 ex[2]^4 ex[3]^5 ex[5]+3 ex[1]^3 ex[2]^5 ex[3]^5 ex[5]+2 ex[1]^3 ex[2]^2 ex[3]^6 ex[5]+6 ex[1]^3 ex[2]^3 ex[3]^6 ex[5]+6 ex[1]^3 ex[2]^4 ex[3]^6 ex[5]+2 ex[1]^3 ex[2]^5 ex[3]^6 ex[5]))/((1+ex[3])^2 (-ex[2] ex[3]+ex[3] ex[4]-ex[5]-ex[3] ex[5]))+(64 ex[3]^2 (ex[1]^3 ex[2]^2 ex[5]^2+3 ex[1]^3 ex[2]^2 ex[3] ex[5]^2+3 ex[1]^3 ex[2]^3 ex[3] ex[5]^2+3 ex[1]^3 ex[2]^2 ex[3]^2 ex[5]^2+6 ex[1]^3 ex[2]^3 ex[3]^2 ex[5]^2+3 ex[1]^3 ex[2]^4 ex[3]^2 ex[5]^2+ex[1]^3 ex[2]^2 ex[3]^3 ex[5]^2+3 ex[1]^3 ex[2]^3 ex[3]^3 ex[5]^2+3 ex[1]^3 ex[2]^4 ex[3]^3 ex[5]^2+ex[1]^3 ex[2]^5 ex[3]^3 ex[5]^2))/((1+ex[3]) (-ex[2] ex[3]+ex[3] ex[4]-ex[5]-ex[3] ex[5])^2);
coeffLogs25=(64 ex[1]^3 ex[2]^4 ex[3]^2 (1+ex[3]+ex[2] ex[3]))/((-1+ex[2]-ex[3]) (1+ex[3])^2)+(64 (1+ex[3]+ex[2] ex[3]) (-ex[1]^3 ex[2]^6 ex[3]^2+ex[1]^3 ex[2]^6 ex[3]^2 ex[5]))/((-1+ex[2]-ex[3]) (1+ex[3])^2 (ex[2]+ex[2] ex[3]-ex[4]+ex[2] ex[4]-ex[3] ex[4]-ex[2] ex[5]-ex[2] ex[3] ex[5]))+(64 (1+ex[3]+ex[2] ex[3]) (-ex[1]^3 ex[2]^4 ex[3]^2 ex[5]-ex[1]^3 ex[2]^5 ex[3]^2 ex[5]+2 ex[1]^3 ex[2]^4 ex[3]^3 ex[5]+2 ex[1]^3 ex[2]^5 ex[3]^3 ex[5]))/(-ex[2] ex[3]-ex[2]^2 ex[3]+ex[4]+ex[2] ex[4]+ex[3] ex[4]+ex[2] ex[3] ex[4]-ex[2] ex[5])-(64 (ex[1]^3 ex[2]^5 ex[3]^2 ex[5]^2+2 ex[1]^3 ex[2]^5 ex[3]^3 ex[5]^2+2 ex[1]^3 ex[2]^6 ex[3]^3 ex[5]^2+ex[1]^3 ex[2]^5 ex[3]^4 ex[5]^2+2 ex[1]^3 ex[2]^6 ex[3]^4 ex[5]^2+ex[1]^3 ex[2]^7 ex[3]^4 ex[5]^2))/(-ex[2] ex[3]-ex[2]^2 ex[3]+ex[4]+ex[2] ex[4]+ex[3] ex[4]+ex[2] ex[3] ex[4]-ex[2] ex[5])^2;
coeffLogs15= (64 ex[1]^3 ex[2]^2 ex[3]^3 (-ex[2]^6 ex[3]^2+2 ex[2]^5 ex[3] ex[4]+3 ex[2]^5 ex[3]^2 ex[4]-ex[2]^4 ex[4]^2-4 ex[2]^4 ex[3] ex[4]^2-3 ex[2]^4 ex[3]^2 ex[4]^2+ex[2]^3 ex[4]^3+2 ex[2]^3 ex[3] ex[4]^3+ex[2]^3 ex[3]^2 ex[4]^3+2 ex[2]^2 ex[3]^2 ex[5]+3 ex[2]^3 ex[3]^2 ex[5]-3 ex[2]^5 ex[3]^2 ex[5]+ex[2]^6 ex[3]^2 ex[5]+6 ex[2]^2 ex[3]^3 ex[5]+12 ex[2]^3 ex[3]^3 ex[5]+6 ex[2]^4 ex[3]^3 ex[5]+6 ex[2]^2 ex[3]^4 ex[5]+15 ex[2]^3 ex[3]^4 ex[5]+12 ex[2]^4 ex[3]^4 ex[5]+3 ex[2]^5 ex[3]^4 ex[5]+2 ex[2]^2 ex[3]^5 ex[5]+6 ex[2]^3 ex[3]^5 ex[5]+6 ex[2]^4 ex[3]^5 ex[5]+2 ex[2]^5 ex[3]^5 ex[5]-4 ex[2] ex[3] ex[4] ex[5]-8 ex[2]^2 ex[3] ex[4] ex[5]-3 ex[2]^3 ex[3] ex[4] ex[5]+4 ex[2]^4 ex[3] ex[4] ex[5]-ex[2]^5 ex[3] ex[4] ex[5]-16 ex[2] ex[3]^2 ex[4] ex[5]-36 ex[2]^2 ex[3]^2 ex[4] ex[5]-24 ex[2]^3 ex[3]^2 ex[4] ex[5]-2 ex[2]^5 ex[3]^2 ex[4] ex[5]-24 ex[2] ex[3]^3 ex[4] ex[5]-60 ex[2]^2 ex[3]^3 ex[4] ex[5]-51 ex[2]^3 ex[3]^3 ex[4] ex[5]-18 ex[2]^4 ex[3]^3 ex[4] ex[5]-3 ex[2]^5 ex[3]^3 ex[4] ex[5]-16 ex[2] ex[3]^4 ex[4] ex[5]-44 ex[2]^2 ex[3]^4 ex[4] ex[5]-42 ex[2]^3 ex[3]^4 ex[4] ex[5]-16 ex[2]^4 ex[3]^4 ex[4] ex[5]-2 ex[2]^5 ex[3]^4 ex[4] ex[5]-4 ex[2] ex[3]^5 ex[4] ex[5]-12 ex[2]^2 ex[3]^5 ex[4] ex[5]-12 ex[2]^3 ex[3]^5 ex[4] ex[5]-4 ex[2]^4 ex[3]^5 ex[4] ex[5]+2 ex[4]^2 ex[5]+5 ex[2] ex[4]^2 ex[5]+3 ex[2]^2 ex[4]^2 ex[5]-ex[2]^3 ex[4]^2 ex[5]+10 ex[3] ex[4]^2 ex[5]+26 ex[2] ex[3] ex[4]^2 ex[5]+21 ex[2]^2 ex[3] ex[4]^2 ex[5]+2 ex[2]^3 ex[3] ex[4]^2 ex[5]+ex[2]^4 ex[3] ex[4]^2 ex[5]+20 ex[3]^2 ex[4]^2 ex[5]+54 ex[2] ex[3]^2 ex[4]^2 ex[5]+51 ex[2]^2 ex[3]^2 ex[4]^2 ex[5]+18 ex[2]^3 ex[3]^2 ex[4]^2 ex[5]+4 ex[2]^4 ex[3]^2 ex[4]^2 ex[5]+20 ex[3]^3 ex[4]^2 ex[5]+56 ex[2] ex[3]^3 ex[4]^2 ex[5]+57 ex[2]^2 ex[3]^3 ex[4]^2 ex[5]+26 ex[2]^3 ex[3]^3 ex[4]^2 ex[5]+5 ex[2]^4 ex[3]^3 ex[4]^2 ex[5]+10 ex[3]^4 ex[4]^2 ex[5]+29 ex[2] ex[3]^4 ex[4]^2 ex[5]+30 ex[2]^2 ex[3]^4 ex[4]^2 ex[5]+13 ex[2]^3 ex[3]^4 ex[4]^2 ex[5]+2 ex[2]^4 ex[3]^4 ex[4]^2 ex[5]+2 ex[3]^5 ex[4]^2 ex[5]+6 ex[2] ex[3]^5 ex[4]^2 ex[5]+6 ex[2]^2 ex[3]^5 ex[4]^2 ex[5]+2 ex[2]^3 ex[3]^5 ex[4]^2 ex[5]+ex[2] ex[3]^2 ex[5]^2-2 ex[2]^2 ex[3]^2 ex[5]^2-6 ex[2]^3 ex[3]^2 ex[5]^2-3 ex[2]^4 ex[3]^2 ex[5]^2+3 ex[2]^5 ex[3]^2 ex[5]^2+4 ex[2] ex[3]^3 ex[5]^2-12 ex[2]^3 ex[3]^3 ex[5]^2-8 ex[2]^4 ex[3]^3 ex[5]^2+5 ex[2] ex[3]^4 ex[5]^2+6 ex[2]^2 ex[3]^4 ex[5]^2-6 ex[2]^3 ex[3]^4 ex[5]^2-10 ex[2]^4 ex[3]^4 ex[5]^2-3 ex[2]^5 ex[3]^4 ex[5]^2+2 ex[2] ex[3]^5 ex[5]^2+4 ex[2]^2 ex[3]^5 ex[5]^2-4 ex[2]^4 ex[3]^5 ex[5]^2-2 ex[2]^5 ex[3]^5 ex[5]^2-ex[3] ex[4] ex[5]^2+ex[2] ex[3] ex[4] ex[5]^2+6 ex[2]^2 ex[3] ex[4] ex[5]^2+5 ex[2]^3 ex[3] ex[4] ex[5]^2-ex[2]^4 ex[3] ex[4] ex[5]^2-5 ex[3]^2 ex[4] ex[5]^2-2 ex[2] ex[3]^2 ex[4] ex[5]^2+12 ex[2]^2 ex[3]^2 ex[4] ex[5]^2+11 ex[2]^3 ex[3]^2 ex[4] ex[5]^2-ex[2]^4 ex[3]^2 ex[4] ex[5]^2-9 ex[3]^3 ex[4] ex[5]^2-11 ex[2] ex[3]^3 ex[4] ex[5]^2+6 ex[2]^2 ex[3]^3 ex[4] ex[5]^2+9 ex[2]^3 ex[3]^3 ex[4] ex[5]^2+ex[2]^4 ex[3]^3 ex[4] ex[5]^2-7 ex[3]^4 ex[4] ex[5]^2-12 ex[2] ex[3]^4 ex[4] ex[5]^2+8 ex[2]^3 ex[3]^4 ex[4] ex[5]^2+3 ex[2]^4 ex[3]^4 ex[4] ex[5]^2-2 ex[3]^5 ex[4] ex[5]^2-4 ex[2] ex[3]^5 ex[4] ex[5]^2+4 ex[2]^3 ex[3]^5 ex[4] ex[5]^2+2 ex[2]^4 ex[3]^5 ex[4] ex[5]^2-ex[2] ex[3]^2 ex[5]^3+3 ex[2]^3 ex[3]^2 ex[5]^3+3 ex[2]^4 ex[3]^2 ex[5]^3-4 ex[2] ex[3]^3 ex[5]^3-6 ex[2]^2 ex[3]^3 ex[5]^3+2 ex[2]^4 ex[3]^3 ex[5]^3-5 ex[2] ex[3]^4 ex[5]^3-12 ex[2]^2 ex[3]^4 ex[5]^3-9 ex[2]^3 ex[3]^4 ex[5]^3-2 ex[2]^4 ex[3]^4 ex[5]^3-2 ex[2] ex[3]^5 ex[5]^3-6 ex[2]^2 ex[3]^5 ex[5]^3-6 ex[2]^3 ex[3]^5 ex[5]^3-2 ex[2]^4 ex[3]^5 ex[5]^3))/((1+ex[3])^2 (ex[2] ex[3]-ex[4]-ex[3] ex[4]+ex[3] ex[5])^2 (-ex[2] ex[3]+ex[4]+ex[2] ex[4]+ex[3] ex[4]+ex[2] ex[3] ex[5]));
coeffLogs13=-((64 ex[1]^3 ex[2]^4 ex[3]^2 (-ex[2]^4 ex[3]^2-2 ex[2]^5 ex[3]^2-ex[2]^6 ex[3]^2-2 ex[2]^4 ex[3]^3-6 ex[2]^5 ex[3]^3-6 ex[2]^6 ex[3]^3-2 ex[2]^7 ex[3]^3-ex[2]^4 ex[3]^4-4 ex[2]^5 ex[3]^4-6 ex[2]^6 ex[3]^4-4 ex[2]^7 ex[3]^4-ex[2]^8 ex[3]^4+2 ex[2]^3 ex[3] ex[4]+4 ex[2]^4 ex[3] ex[4]+2 ex[2]^5 ex[3] ex[4]+8 ex[2]^3 ex[3]^2 ex[4]+20 ex[2]^4 ex[3]^2 ex[4]+16 ex[2]^5 ex[3]^2 ex[4]+4 ex[2]^6 ex[3]^2 ex[4]+10 ex[2]^3 ex[3]^3 ex[4]+32 ex[2]^4 ex[3]^3 ex[4]+36 ex[2]^5 ex[3]^3 ex[4]+16 ex[2]^6 ex[3]^3 ex[4]+2 ex[2]^7 ex[3]^3 ex[4]+4 ex[2]^3 ex[3]^4 ex[4]+16 ex[2]^4 ex[3]^4 ex[4]+24 ex[2]^5 ex[3]^4 ex[4]+16 ex[2]^6 ex[3]^4 ex[4]+4 ex[2]^7 ex[3]^4 ex[4]-ex[2]^2 ex[4]^2-2 ex[2]^3 ex[4]^2-ex[2]^4 ex[4]^2-8 ex[2]^2 ex[3] ex[4]^2-18 ex[2]^3 ex[3] ex[4]^2-12 ex[2]^4 ex[3] ex[4]^2-2 ex[2]^5 ex[3] ex[4]^2-19 ex[2]^2 ex[3]^2 ex[4]^2-52 ex[2]^3 ex[3]^2 ex[4]^2-48 ex[2]^4 ex[3]^2 ex[4]^2-16 ex[2]^5 ex[3]^2 ex[4]^2-ex[2]^6 ex[3]^2 ex[4]^2-18 ex[2]^2 ex[3]^3 ex[4]^2-60 ex[2]^3 ex[3]^3 ex[4]^2-72 ex[2]^4 ex[3]^3 ex[4]^2-36 ex[2]^5 ex[3]^3 ex[4]^2-6 ex[2]^6 ex[3]^3 ex[4]^2-6 ex[2]^2 ex[3]^4 ex[4]^2-24 ex[2]^3 ex[3]^4 ex[4]^2-36 ex[2]^4 ex[3]^4 ex[4]^2-24 ex[2]^5 ex[3]^4 ex[4]^2-6 ex[2]^6 ex[3]^4 ex[4]^2+2 ex[2] ex[4]^3+4 ex[2]^2 ex[4]^3+2 ex[2]^3 ex[4]^3+10 ex[2] ex[3] ex[4]^3+24 ex[2]^2 ex[3] ex[4]^3+18 ex[2]^3 ex[3] ex[4]^3+4 ex[2]^4 ex[3] ex[4]^3+18 ex[2] ex[3]^2 ex[4]^3+52 ex[2]^2 ex[3]^2 ex[4]^3+52 ex[2]^3 ex[3]^2 ex[4]^3+20 ex[2]^4 ex[3]^2 ex[4]^3+2 ex[2]^5 ex[3]^2 ex[4]^3+14 ex[2] ex[3]^3 ex[4]^3+48 ex[2]^2 ex[3]^3 ex[4]^3+60 ex[2]^3 ex[3]^3 ex[4]^3+32 ex[2]^4 ex[3]^3 ex[4]^3+6 ex[2]^5 ex[3]^3 ex[4]^3+4 ex[2] ex[3]^4 ex[4]^3+16 ex[2]^2 ex[3]^4 ex[4]^3+24 ex[2]^3 ex[3]^4 ex[4]^3+16 ex[2]^4 ex[3]^4 ex[4]^3+4 ex[2]^5 ex[3]^4 ex[4]^3-ex[4]^4-2 ex[2] ex[4]^4-ex[2]^2 ex[4]^4-4 ex[3] ex[4]^4-10 ex[2] ex[3] ex[4]^4-8 ex[2]^2 ex[3] ex[4]^4-2 ex[2]^3 ex[3] ex[4]^4-6 ex[3]^2 ex[4]^4-18 ex[2] ex[3]^2 ex[4]^4-19 ex[2]^2 ex[3]^2 ex[4]^4-8 ex[2]^3 ex[3]^2 ex[4]^4-ex[2]^4 ex[3]^2 ex[4]^4-4 ex[3]^3 ex[4]^4-14 ex[2] ex[3]^3 ex[4]^4-18 ex[2]^2 ex[3]^3 ex[4]^4-10 ex[2]^3 ex[3]^3 ex[4]^4-2 ex[2]^4 ex[3]^3 ex[4]^4-ex[3]^4 ex[4]^4-4 ex[2] ex[3]^4 ex[4]^4-6 ex[2]^2 ex[3]^4 ex[4]^4-4 ex[2]^3 ex[3]^4 ex[4]^4-ex[2]^4 ex[3]^4 ex[4]^4+ex[2]^3 ex[3] ex[5]-ex[2]^5 ex[3] ex[5]+3 ex[2]^3 ex[3]^2 ex[5]+7 ex[2]^4 ex[3]^2 ex[5]+5 ex[2]^5 ex[3]^2 ex[5]+ex[2]^6 ex[3]^2 ex[5]+6 ex[2]^4 ex[3]^3 ex[5]+15 ex[2]^5 ex[3]^3 ex[5]+12 ex[2]^6 ex[3]^3 ex[5]+3 ex[2]^7 ex[3]^3 ex[5]-7 ex[2]^3 ex[3]^4 ex[5]-17 ex[2]^4 ex[3]^4 ex[5]-10 ex[2]^5 ex[3]^4 ex[5]+4 ex[2]^6 ex[3]^4 ex[5]+5 ex[2]^7 ex[3]^4 ex[5]+ex[2]^8 ex[3]^4 ex[5]-7 ex[2]^3 ex[3]^5 ex[5]-24 ex[2]^4 ex[3]^5 ex[5]-30 ex[2]^5 ex[3]^5 ex[5]-16 ex[2]^6 ex[3]^5 ex[5]-3 ex[2]^7 ex[3]^5 ex[5]-2 ex[2]^3 ex[3]^6 ex[5]-8 ex[2]^4 ex[3]^6 ex[5]-12 ex[2]^5 ex[3]^6 ex[5]-8 ex[2]^6 ex[3]^6 ex[5]-2 ex[2]^7 ex[3]^6 ex[5]-ex[2]^2 ex[4] ex[5]+ex[2]^4 ex[4] ex[5]-6 ex[2]^2 ex[3] ex[4] ex[5]-9 ex[2]^3 ex[3] ex[4] ex[5]-6 ex[2]^4 ex[3] ex[4] ex[5]-3 ex[2]^5 ex[3] ex[4] ex[5]-9 ex[2]^2 ex[3]^2 ex[4] ex[5]-29 ex[2]^3 ex[3]^2 ex[4] ex[5]-36 ex[2]^4 ex[3]^2 ex[4] ex[5]-21 ex[2]^5 ex[3]^2 ex[4] ex[5]-5 ex[2]^6 ex[3]^2 ex[4] ex[5]+4 ex[2]^2 ex[3]^3 ex[4] ex[5]-10 ex[2]^3 ex[3]^3 ex[4] ex[5]-44 ex[2]^4 ex[3]^3 ex[4] ex[5]-43 ex[2]^5 ex[3]^3 ex[4] ex[5]-14 ex[2]^6 ex[3]^3 ex[4] ex[5]-ex[2]^7 ex[3]^3 ex[4] ex[5]+20 ex[2]^2 ex[3]^4 ex[4] ex[5]+45 ex[2]^3 ex[3]^4 ex[4] ex[5]+18 ex[2]^4 ex[3]^4 ex[4] ex[5]-22 ex[2]^5 ex[3]^4 ex[4] ex[5]-18 ex[2]^6 ex[3]^4 ex[4] ex[5]-3 ex[2]^7 ex[3]^4 ex[4] ex[5]+16 ex[2]^2 ex[3]^5 ex[4] ex[5]+49 ex[2]^3 ex[3]^5 ex[4] ex[5]+48 ex[2]^4 ex[3]^5 ex[4] ex[5]+10 ex[2]^5 ex[3]^5 ex[4] ex[5]-8 ex[2]^6 ex[3]^5 ex[4] ex[5]-3 ex[2]^7 ex[3]^5 ex[4] ex[5]+4 ex[2]^2 ex[3]^6 ex[4] ex[5]+14 ex[2]^3 ex[3]^6 ex[4] ex[5]+16 ex[2]^4 ex[3]^6 ex[4] ex[5]+4 ex[2]^5 ex[3]^6 ex[4] ex[5]-4 ex[2]^6 ex[3]^6 ex[4] ex[5]-2 ex[2]^7 ex[3]^6 ex[4] ex[5]+2 ex[2] ex[4]^2 ex[5]+2 ex[2]^2 ex[4]^2 ex[5]+2 ex[2]^3 ex[4]^2 ex[5]+2 ex[2]^4 ex[4]^2 ex[5]+9 ex[2] ex[3] ex[4]^2 ex[5]+20 ex[2]^2 ex[3] ex[4]^2 ex[5]+21 ex[2]^3 ex[3] ex[4]^2 ex[5]+12 ex[2]^4 ex[3] ex[4]^2 ex[5]+2 ex[2]^5 ex[3] ex[4]^2 ex[5]+12 ex[2] ex[3]^2 ex[4]^2 ex[5]+46 ex[2]^2 ex[3]^2 ex[4]^2 ex[5]+66 ex[2]^3 ex[3]^2 ex[4]^2 ex[5]+42 ex[2]^4 ex[3]^2 ex[4]^2 ex[5]+10 ex[2]^5 ex[3]^2 ex[4]^2 ex[5]+32 ex[2]^2 ex[3]^3 ex[4]^2 ex[5]+85 ex[2]^3 ex[3]^3 ex[4]^2 ex[5]+76 ex[2]^4 ex[3]^3 ex[4]^2 ex[5]+25 ex[2]^5 ex[3]^3 ex[4]^2 ex[5]+2 ex[2]^6 ex[3]^3 ex[4]^2 ex[5]-12 ex[2] ex[3]^4 ex[4]^2 ex[5]-8 ex[2]^2 ex[3]^4 ex[4]^2 ex[5]+48 ex[2]^3 ex[3]^4 ex[4]^2 ex[5]+78 ex[2]^4 ex[3]^4 ex[4]^2 ex[5]+40 ex[2]^5 ex[3]^4 ex[4]^2 ex[5]+6 ex[2]^6 ex[3]^4 ex[4]^2 ex[5]-9 ex[2] ex[3]^5 ex[4]^2 ex[5]-16 ex[2]^2 ex[3]^5 ex[4]^2 ex[5]+14 ex[2]^3 ex[3]^5 ex[4]^2 ex[5]+48 ex[2]^4 ex[3]^5 ex[4]^2 ex[5]+35 ex[2]^5 ex[3]^5 ex[4]^2 ex[5]+8 ex[2]^6 ex[3]^5 ex[4]^2 ex[5]-2 ex[2] ex[3]^6 ex[4]^2 ex[5]-4 ex[2]^2 ex[3]^6 ex[4]^2 ex[5]+4 ex[2]^3 ex[3]^6 ex[4]^2 ex[5]+16 ex[2]^4 ex[3]^6 ex[4]^2 ex[5]+14 ex[2]^5 ex[3]^6 ex[4]^2 ex[5]+4 ex[2]^6 ex[3]^6 ex[4]^2 ex[5]-ex[4]^3 ex[5]-2 ex[2] ex[4]^3 ex[5]-3 ex[2]^2 ex[4]^3 ex[5]-2 ex[2]^3 ex[4]^3 ex[5]-4 ex[3] ex[4]^3 ex[5]-11 ex[2] ex[3] ex[4]^3 ex[5]-14 ex[2]^2 ex[3] ex[4]^3 ex[5]-9 ex[2]^3 ex[3] ex[4]^3 ex[5]-2 ex[2]^4 ex[3] ex[4]^3 ex[5]-6 ex[3]^2 ex[4]^3 ex[5]-24 ex[2] ex[3]^2 ex[4]^3 ex[5]-35 ex[2]^2 ex[3]^2 ex[4]^3 ex[5]-22 ex[2]^3 ex[3]^2 ex[4]^3 ex[5]-5 ex[2]^4 ex[3]^2 ex[4]^3 ex[5]-4 ex[3]^3 ex[4]^3 ex[5]-28 ex[2] ex[3]^3 ex[4]^3 ex[5]-56 ex[2]^2 ex[3]^3 ex[4]^3 ex[5]-45 ex[2]^3 ex[3]^3 ex[4]^3 ex[5]-14 ex[2]^4 ex[3]^3 ex[4]^3 ex[5]-ex[2]^5 ex[3]^3 ex[4]^3 ex[5]-ex[3]^4 ex[4]^3 ex[5]-20 ex[2] ex[3]^4 ex[4]^3 ex[5]-56 ex[2]^2 ex[3]^4 ex[4]^3 ex[5]-60 ex[2]^3 ex[3]^4 ex[4]^3 ex[5]-27 ex[2]^4 ex[3]^4 ex[4]^3 ex[5]-4 ex[2]^5 ex[3]^4 ex[4]^3 ex[5]-9 ex[2] ex[3]^5 ex[4]^3 ex[5]-32 ex[2]^2 ex[3]^5 ex[4]^3 ex[5]-42 ex[2]^3 ex[3]^5 ex[4]^3 ex[5]-24 ex[2]^4 ex[3]^5 ex[4]^3 ex[5]-5 ex[2]^5 ex[3]^5 ex[4]^3 ex[5]-2 ex[2] ex[3]^6 ex[4]^3 ex[5]-8 ex[2]^2 ex[3]^6 ex[4]^3 ex[5]-12 ex[2]^3 ex[3]^6 ex[4]^3 ex[5]-8 ex[2]^4 ex[3]^6 ex[4]^3 ex[5]-2 ex[2]^5 ex[3]^6 ex[4]^3 ex[5]-5 ex[2]^3 ex[3] ex[5]^2+3 ex[2]^5 ex[3] ex[5]^2-19 ex[2]^3 ex[3]^2 ex[5]^2-24 ex[2]^4 ex[3]^2 ex[5]^2-5 ex[2]^5 ex[3]^2 ex[5]^2+ex[2]^6 ex[3]^2 ex[5]^2-20 ex[2]^3 ex[3]^3 ex[5]^2-40 ex[2]^4 ex[3]^3 ex[5]^2-27 ex[2]^5 ex[3]^3 ex[5]^2-8 ex[2]^6 ex[3]^3 ex[5]^2-ex[2]^7 ex[3]^3 ex[5]^2-ex[2]^3 ex[3]^4 ex[5]^2+2 ex[2]^5 ex[3]^4 ex[5]^2-ex[2]^7 ex[3]^4 ex[5]^2+7 ex[2]^3 ex[3]^5 ex[5]^2+24 ex[2]^4 ex[3]^5 ex[5]^2+30 ex[2]^5 ex[3]^5 ex[5]^2+16 ex[2]^6 ex[3]^5 ex[5]^2+3 ex[2]^7 ex[3]^5 ex[5]^2+2 ex[2]^3 ex[3]^6 ex[5]^2+8 ex[2]^4 ex[3]^6 ex[5]^2+12 ex[2]^5 ex[3]^6 ex[5]^2+8 ex[2]^6 ex[3]^6 ex[5]^2+2 ex[2]^7 ex[3]^6 ex[5]^2+2 ex[2]^2 ex[4] ex[5]^2-2 ex[2]^4 ex[4] ex[5]^2+14 ex[2]^2 ex[3] ex[4] ex[5]^2+10 ex[2]^3 ex[3] ex[4] ex[5]^2+2 ex[2]^4 ex[3] ex[4] ex[5]^2+2 ex[2]^5 ex[3] ex[4] ex[5]^2+28 ex[2]^2 ex[3]^2 ex[4] ex[5]^2+32 ex[2]^3 ex[3]^2 ex[4] ex[5]^2+10 ex[2]^4 ex[3]^2 ex[4] ex[5]^2+6 ex[2]^5 ex[3]^2 ex[4] ex[5]^2+2 ex[2]^6 ex[3]^2 ex[4] ex[5]^2+14 ex[2]^2 ex[3]^3 ex[4] ex[5]^2+10 ex[2]^3 ex[3]^3 ex[4] ex[5]^2-8 ex[2]^4 ex[3]^3 ex[4] ex[5]^2-2 ex[2]^5 ex[3]^3 ex[4] ex[5]^2+2 ex[2]^6 ex[3]^3 ex[4] ex[5]^2-14 ex[2]^2 ex[3]^4 ex[4] ex[5]^2-52 ex[2]^3 ex[3]^4 ex[4] ex[5]^2-64 ex[2]^4 ex[3]^4 ex[4] ex[5]^2-28 ex[2]^5 ex[3]^4 ex[4] ex[5]^2-2 ex[2]^6 ex[3]^4 ex[4] ex[5]^2-16 ex[2]^2 ex[3]^5 ex[4] ex[5]^2-56 ex[2]^3 ex[3]^5 ex[4] ex[5]^2-72 ex[2]^4 ex[3]^5 ex[4] ex[5]^2-40 ex[2]^5 ex[3]^5 ex[4] ex[5]^2-8 ex[2]^6 ex[3]^5 ex[4] ex[5]^2-4 ex[2]^2 ex[3]^6 ex[4] ex[5]^2-16 ex[2]^3 ex[3]^6 ex[4] ex[5]^2-24 ex[2]^4 ex[3]^6 ex[4] ex[5]^2-16 ex[2]^5 ex[3]^6 ex[4] ex[5]^2-4 ex[2]^6 ex[3]^6 ex[4] ex[5]^2-2 ex[2] ex[4]^2 ex[5]^2-ex[2]^2 ex[4]^2 ex[5]^2-ex[2]^4 ex[4]^2 ex[5]^2-9 ex[2] ex[3] ex[4]^2 ex[5]^2-10 ex[2]^2 ex[3] ex[4]^2 ex[5]^2-5 ex[2]^3 ex[3] ex[4]^2 ex[5]^2-2 ex[2]^4 ex[3] ex[4]^2 ex[5]^2-12 ex[2] ex[3]^2 ex[4]^2 ex[5]^2-17 ex[2]^2 ex[3]^2 ex[4]^2 ex[5]^2-14 ex[2]^3 ex[3]^2 ex[4]^2 ex[5]^2-10 ex[2]^4 ex[3]^2 ex[4]^2 ex[5]^2-2 ex[2]^5 ex[3]^2 ex[4]^2 ex[5]^2+8 ex[2]^2 ex[3]^3 ex[4]^2 ex[5]^2+5 ex[2]^3 ex[3]^3 ex[4]^2 ex[5]^2-8 ex[2]^4 ex[3]^3 ex[4]^2 ex[5]^2-5 ex[2]^5 ex[3]^3 ex[4]^2 ex[5]^2+12 ex[2] ex[3]^4 ex[4]^2 ex[5]^2+40 ex[2]^2 ex[3]^4 ex[4]^2 ex[5]^2+44 ex[2]^3 ex[3]^4 ex[4]^2 ex[5]^2+16 ex[2]^4 ex[3]^4 ex[4]^2 ex[5]^2+9 ex[2] ex[3]^5 ex[4]^2 ex[5]^2+32 ex[2]^2 ex[3]^5 ex[4]^2 ex[5]^2+42 ex[2]^3 ex[3]^5 ex[4]^2 ex[5]^2+24 ex[2]^4 ex[3]^5 ex[4]^2 ex[5]^2+5 ex[2]^5 ex[3]^5 ex[4]^2 ex[5]^2+2 ex[2] ex[3]^6 ex[4]^2 ex[5]^2+8 ex[2]^2 ex[3]^6 ex[4]^2 ex[5]^2+12 ex[2]^3 ex[3]^6 ex[4]^2 ex[5]^2+8 ex[2]^4 ex[3]^6 ex[4]^2 ex[5]^2+2 ex[2]^5 ex[3]^6 ex[4]^2 ex[5]^2+7 ex[2]^3 ex[3] ex[5]^3-3 ex[2]^5 ex[3] ex[5]^3+28 ex[2]^3 ex[3]^2 ex[5]^3+30 ex[2]^4 ex[3]^2 ex[5]^3+3 ex[2]^5 ex[3]^2 ex[5]^3-ex[2]^6 ex[3]^2 ex[5]^3+35 ex[2]^3 ex[3]^3 ex[5]^3+60 ex[2]^4 ex[3]^3 ex[5]^3+27 ex[2]^5 ex[3]^3 ex[5]^3+2 ex[2]^6 ex[3]^3 ex[5]^3+14 ex[2]^3 ex[3]^4 ex[5]^3+30 ex[2]^4 ex[3]^4 ex[5]^3+18 ex[2]^5 ex[3]^4 ex[5]^3+2 ex[2]^6 ex[3]^4 ex[5]^3-ex[2]^2 ex[4] ex[5]^3+ex[2]^4 ex[4] ex[5]^3-8 ex[2]^2 ex[3] ex[4] ex[5]^3-3 ex[2]^3 ex[3] ex[4] ex[5]^3-ex[2]^5 ex[3] ex[4] ex[5]^3-19 ex[2]^2 ex[3]^2 ex[4] ex[5]^3-12 ex[2]^3 ex[3]^2 ex[4] ex[5]^3+6 ex[2]^4 ex[3]^2 ex[4] ex[5]^3+ex[2]^5 ex[3]^2 ex[4] ex[5]^3-18 ex[2]^2 ex[3]^3 ex[4] ex[5]^3-15 ex[2]^3 ex[3]^3 ex[4] ex[5]^3+12 ex[2]^4 ex[3]^3 ex[4] ex[5]^3+9 ex[2]^5 ex[3]^3 ex[4] ex[5]^3-6 ex[2]^2 ex[3]^4 ex[4] ex[5]^3-6 ex[2]^3 ex[3]^4 ex[4] ex[5]^3+6 ex[2]^4 ex[3]^4 ex[4] ex[5]^3+6 ex[2]^5 ex[3]^4 ex[4] ex[5]^3-3 ex[2]^3 ex[3] ex[5]^4+ex[2]^5 ex[3] ex[5]^4-12 ex[2]^3 ex[3]^2 ex[5]^4-12 ex[2]^4 ex[3]^2 ex[5]^4-ex[2]^5 ex[3]^2 ex[5]^4-15 ex[2]^3 ex[3]^3 ex[5]^4-24 ex[2]^4 ex[3]^3 ex[5]^4-9 ex[2]^5 ex[3]^3 ex[5]^4-6 ex[2]^3 ex[3]^4 ex[5]^4-12 ex[2]^4 ex[3]^4 ex[5]^4-6 ex[2]^5 ex[3]^4 ex[5]^4))/((1+ex[3])^2 (ex[2]+ex[2] ex[3]+ex[2]^2 ex[3]-ex[4]-ex[3] ex[4]-ex[2] ex[3] ex[4]-ex[2] ex[5])^2 (ex[2] ex[3]+ex[2]^2 ex[3]-ex[4]-ex[2] ex[4]-ex[3] ex[4]-ex[2] ex[3] ex[4]+ex[2] ex[5])^2));
coeffLogs23=-((64 ex[1]^3 ex[2]^2 ex[3]^2 (-ex[2]^6 ex[3]^4+2 ex[2]^5 ex[3]^3 ex[4]+4 ex[2]^5 ex[3]^4 ex[4]-ex[2]^4 ex[3]^2 ex[4]^2-6 ex[2]^4 ex[3]^3 ex[4]^2-6 ex[2]^4 ex[3]^4 ex[4]^2+2 ex[2]^3 ex[3]^2 ex[4]^3+6 ex[2]^3 ex[3]^3 ex[4]^3+4 ex[2]^3 ex[3]^4 ex[4]^3-ex[2]^2 ex[3]^2 ex[4]^4-2 ex[2]^2 ex[3]^3 ex[4]^4-ex[2]^2 ex[3]^4 ex[4]^4-2 ex[2]^5 ex[3]^3 ex[5]-4 ex[2]^5 ex[3]^4 ex[5]+ex[2]^6 ex[3]^4 ex[5]+4 ex[2]^4 ex[3]^2 ex[4] ex[5]-2 ex[2]^2 ex[3]^3 ex[4] ex[5]-3 ex[2]^3 ex[3]^3 ex[4] ex[5]+12 ex[2]^4 ex[3]^3 ex[4] ex[5]-ex[2]^5 ex[3]^3 ex[4] ex[5]-6 ex[2]^2 ex[3]^4 ex[4] ex[5]-12 ex[2]^3 ex[3]^4 ex[4] ex[5]+6 ex[2]^4 ex[3]^4 ex[4] ex[5]-3 ex[2]^5 ex[3]^4 ex[4] ex[5]-6 ex[2]^2 ex[3]^5 ex[4] ex[5]-15 ex[2]^3 ex[3]^5 ex[4] ex[5]-12 ex[2]^4 ex[3]^5 ex[4] ex[5]-3 ex[2]^5 ex[3]^5 ex[4] ex[5]-2 ex[2]^2 ex[3]^6 ex[4] ex[5]-6 ex[2]^3 ex[3]^6 ex[4] ex[5]-6 ex[2]^4 ex[3]^6 ex[4] ex[5]-2 ex[2]^5 ex[3]^6 ex[4] ex[5]-2 ex[2]^3 ex[3] ex[4]^2 ex[5]+2 ex[2] ex[3]^2 ex[4]^2 ex[5]+3 ex[2]^2 ex[3]^2 ex[4]^2 ex[5]-10 ex[2]^3 ex[3]^2 ex[4]^2 ex[5]+10 ex[2] ex[3]^3 ex[4]^2 ex[5]+18 ex[2]^2 ex[3]^3 ex[4]^2 ex[5]-12 ex[2]^3 ex[3]^3 ex[4]^2 ex[5]+2 ex[2]^4 ex[3]^3 ex[4]^2 ex[5]+18 ex[2] ex[3]^4 ex[4]^2 ex[5]+39 ex[2]^2 ex[3]^4 ex[4]^2 ex[5]+12 ex[2]^3 ex[3]^4 ex[4]^2 ex[5]+6 ex[2]^4 ex[3]^4 ex[4]^2 ex[5]+14 ex[2] ex[3]^5 ex[4]^2 ex[5]+36 ex[2]^2 ex[3]^5 ex[4]^2 ex[5]+30 ex[2]^3 ex[3]^5 ex[4]^2 ex[5]+8 ex[2]^4 ex[3]^5 ex[4]^2 ex[5]+4 ex[2] ex[3]^6 ex[4]^2 ex[5]+12 ex[2]^2 ex[3]^6 ex[4]^2 ex[5]+12 ex[2]^3 ex[3]^6 ex[4]^2 ex[5]+4 ex[2]^4 ex[3]^6 ex[4]^2 ex[5]+2 ex[2]^2 ex[3] ex[4]^3 ex[5]-2 ex[3]^2 ex[4]^3 ex[5]-3 ex[2] ex[3]^2 ex[4]^3 ex[5]+6 ex[2]^2 ex[3]^2 ex[4]^3 ex[5]-8 ex[3]^3 ex[4]^3 ex[5]-15 ex[2] ex[3]^3 ex[4]^3 ex[5]+2 ex[2]^2 ex[3]^3 ex[4]^3 ex[5]-ex[2]^3 ex[3]^3 ex[4]^3 ex[5]-12 ex[3]^4 ex[4]^3 ex[5]-27 ex[2] ex[3]^4 ex[4]^3 ex[5]-14 ex[2]^2 ex[3]^4 ex[4]^3 ex[5]-4 ex[2]^3 ex[3]^4 ex[4]^3 ex[5]-8 ex[3]^5 ex[4]^3 ex[5]-21 ex[2] ex[3]^5 ex[4]^3 ex[5]-18 ex[2]^2 ex[3]^5 ex[4]^3 ex[5]-5 ex[2]^3 ex[3]^5 ex[4]^3 ex[5]-2 ex[3]^6 ex[4]^3 ex[5]-6 ex[2] ex[3]^6 ex[4]^3 ex[5]-6 ex[2]^2 ex[3]^6 ex[4]^3 ex[5]-2 ex[2]^3 ex[3]^6 ex[4]^3 ex[5]+ex[2]^2 ex[3]^2 ex[5]^2-ex[2]^4 ex[3]^2 ex[5]^2+2 ex[2]^2 ex[3]^3 ex[5]^2-6 ex[2]^4 ex[3]^3 ex[5]^2+2 ex[2]^5 ex[3]^3 ex[5]^2+ex[2]^2 ex[3]^4 ex[5]^2-6 ex[2]^4 ex[3]^4 ex[5]^2+4 ex[2]^5 ex[3]^4 ex[5]^2-2 ex[2] ex[3] ex[4] ex[5]^2+2 ex[2]^3 ex[3] ex[4] ex[5]^2-10 ex[2] ex[3]^2 ex[4] ex[5]^2-6 ex[2]^2 ex[3]^2 ex[4] ex[5]^2+10 ex[2]^3 ex[3]^2 ex[4] ex[5]^2-2 ex[2]^4 ex[3]^2 ex[4] ex[5]^2-20 ex[2] ex[3]^3 ex[4] ex[5]^2-24 ex[2]^2 ex[3]^3 ex[4] ex[5]^2+12 ex[2]^3 ex[3]^3 ex[4] ex[5]^2-6 ex[2]^4 ex[3]^3 ex[4] ex[5]^2-22 ex[2] ex[3]^4 ex[4] ex[5]^2-42 ex[2]^2 ex[3]^4 ex[4] ex[5]^2-12 ex[2]^3 ex[3]^4 ex[4] ex[5]^2-10 ex[2]^4 ex[3]^4 ex[4] ex[5]^2-14 ex[2] ex[3]^5 ex[4] ex[5]^2-36 ex[2]^2 ex[3]^5 ex[4] ex[5]^2-30 ex[2]^3 ex[3]^5 ex[4] ex[5]^2-8 ex[2]^4 ex[3]^5 ex[4] ex[5]^2-4 ex[2] ex[3]^6 ex[4] ex[5]^2-12 ex[2]^2 ex[3]^6 ex[4] ex[5]^2-12 ex[2]^3 ex[3]^6 ex[4] ex[5]^2-4 ex[2]^4 ex[3]^6 ex[4] ex[5]^2+ex[4]^2 ex[5]^2-ex[2]^2 ex[4]^2 ex[5]^2+8 ex[3] ex[4]^2 ex[5]^2+6 ex[2] ex[3] ex[4]^2 ex[5]^2-4 ex[2]^2 ex[3] ex[4]^2 ex[5]^2+23 ex[3]^2 ex[4]^2 ex[5]^2+30 ex[2] ex[3]^2 ex[4]^2 ex[5]^2-ex[2]^2 ex[3]^2 ex[4]^2 ex[5]^2+2 ex[2]^3 ex[3]^2 ex[4]^2 ex[5]^2+34 ex[3]^3 ex[4]^2 ex[5]^2+60 ex[2] ex[3]^3 ex[4]^2 ex[5]^2+18 ex[2]^2 ex[3]^3 ex[4]^2 ex[5]^2+8 ex[2]^3 ex[3]^3 ex[4]^2 ex[5]^2+30 ex[3]^4 ex[4]^2 ex[5]^2+66 ex[2] ex[3]^4 ex[4]^2 ex[5]^2+39 ex[2]^2 ex[3]^4 ex[4]^2 ex[5]^2+12 ex[2]^3 ex[3]^4 ex[4]^2 ex[5]^2+16 ex[3]^5 ex[4]^2 ex[5]^2+42 ex[2] ex[3]^5 ex[4]^2 ex[5]^2+36 ex[2]^2 ex[3]^5 ex[4]^2 ex[5]^2+10 ex[2]^3 ex[3]^5 ex[4]^2 ex[5]^2+4 ex[3]^6 ex[4]^2 ex[5]^2+12 ex[2] ex[3]^6 ex[4]^2 ex[5]^2+12 ex[2]^2 ex[3]^6 ex[4]^2 ex[5]^2+4 ex[2]^3 ex[3]^6 ex[4]^2 ex[5]^2-3 ex[2]^2 ex[3]^2 ex[5]^3-2 ex[2]^3 ex[3]^2 ex[5]^3+ex[2]^4 ex[3]^2 ex[5]^3-6 ex[2]^2 ex[3]^3 ex[5]^3-6 ex[2]^3 ex[3]^3 ex[5]^3+6 ex[2]^4 ex[3]^3 ex[5]^3-3 ex[2]^2 ex[3]^4 ex[5]^3-4 ex[2]^3 ex[3]^4 ex[5]^3+6 ex[2]^4 ex[3]^4 ex[5]^3+3 ex[2] ex[3] ex[4] ex[5]^3+2 ex[2]^2 ex[3] ex[4] ex[5]^3-ex[2]^3 ex[3] ex[4] ex[5]^3-2 ex[3]^2 ex[4] ex[5]^3+9 ex[2] ex[3]^2 ex[4] ex[5]^3+12 ex[2]^2 ex[3]^2 ex[4] ex[5]^3-3 ex[2]^3 ex[3]^2 ex[4] ex[5]^3-8 ex[3]^3 ex[4] ex[5]^3+14 ex[2]^2 ex[3]^3 ex[4] ex[5]^3-4 ex[2]^3 ex[3]^3 ex[4] ex[5]^3-12 ex[3]^4 ex[4] ex[5]^3-21 ex[2] ex[3]^4 ex[4] ex[5]^3-8 ex[2]^2 ex[3]^4 ex[4] ex[5]^3-6 ex[2]^3 ex[3]^4 ex[4] ex[5]^3-8 ex[3]^5 ex[4] ex[5]^3-21 ex[2] ex[3]^5 ex[4] ex[5]^3-18 ex[2]^2 ex[3]^5 ex[4] ex[5]^3-5 ex[2]^3 ex[3]^5 ex[4] ex[5]^3-2 ex[3]^6 ex[4] ex[5]^3-6 ex[2] ex[3]^6 ex[4] ex[5]^3-6 ex[2]^2 ex[3]^6 ex[4] ex[5]^3-2 ex[2]^3 ex[3]^6 ex[4] ex[5]^3+2 ex[2]^2 ex[3]^2 ex[5]^4+2 ex[2]^3 ex[3]^2 ex[5]^4+4 ex[2]^2 ex[3]^3 ex[5]^4+6 ex[2]^3 ex[3]^3 ex[5]^4+2 ex[2]^2 ex[3]^4 ex[5]^4+4 ex[2]^3 ex[3]^4 ex[5]^4))/((1+ex[3])^2 (ex[2] ex[3]-ex[4]-ex[3] ex[4]+ex[3] ex[5])^2 (ex[2] ex[3]-ex[3] ex[4]+ex[5]+ex[3] ex[5])^2));


(* ::Item:: *)
(*List of all the poles*)


\[CapitalDelta]1523= GetMomentumTwistorExpression[(s[1,5]-s[2,3])/s[1,2], PSanalytic];
\[CapitalDelta]1324= GetMomentumTwistorExpression[(s[2,4]-s[1,3])/s[1,2], PSanalytic];
\[CapitalDelta]1423= GetMomentumTwistorExpression[(s[2,3]-s[1,4])/s[1,2], PSanalytic];
\[CapitalDelta]1325= GetMomentumTwistorExpression[(s[2,5]-s[1,3])/s[1,2], PSanalytic];
\[CapitalDelta]1524= GetMomentumTwistorExpression[(s[1,5]-s[2,4])/s[1,2], PSanalytic];
\[CapitalDelta]1523sub=Solve[\[CapitalDelta]1523==\[Delta], ex[3]][[1,1]];
\[CapitalDelta]1324sub=Solve[\[CapitalDelta]1324==\[Delta], ex[3]][[1,1]];
\[CapitalDelta]1423sub=Solve[\[CapitalDelta]1423==\[Delta], ex[3]][[1,1]];
\[CapitalDelta]1325sub= Solve[\[CapitalDelta]1325==\[Delta], ex[3]][[1,1]];
\[CapitalDelta]1524sub= Solve[\[CapitalDelta]1524==\[Delta], ex[3]][[1,1]];
backFrom\[CapitalDelta]1523=\[Delta]->GetMomentumTwistorExpression[(s[1,5]-s[2,3])/s[1,2], PSanalytic]; 
backFrom\[CapitalDelta]2413= \[Delta]->GetMomentumTwistorExpression[(s[2,4]-s[1,3])/s[1,2], PSanalytic]; 
backFrom\[CapitalDelta]2314=\[Delta]->GetMomentumTwistorExpression[(s[2,3]-s[1,4])/s[1,2], PSanalytic];
backFrom\[CapitalDelta]1325=\[Delta]->GetMomentumTwistorExpression[(s[2,5]-s[1,3])/s[1,2], PSanalytic];
backFrom\[CapitalDelta]1524=\[Delta]->GetMomentumTwistorExpression[(s[1,5]-s[2,4])/s[1,2], PSanalytic];


(* ::Item:: *)
(*Here the list of possible coefficients for specific poles are constructed. This information can be used when guessing different coefficients*)


GetMomentumTwistorExpression[coeffansatz, PSanalytic];
coeffFactorsGuess=Rule[#,Factor[#/.sliceRules]]&/@%;


ansatzSliced=GetMomentumTwistorExpression[coeffansatz, PSanalytic]/.sliceRules;
listofPossibleFactorSliced=DeleteCases[Flatten[Table[(FactorList[#]&/@ansatzSliced)[[i]][[All,1]], {i, Length@ansatzSliced}]], a_ /;Variables[a]==={}] //DeleteDuplicates; 

ansatzMT\[CapitalDelta]1324=GetMomentumTwistorExpression[coeffansatz, PSanalytic] /.\[CapitalDelta]1324sub;
listofPossibleFactors\[CapitalDelta]1324=DeleteCases[Flatten[Table[(FactorList[#]&/@ansatzMT\[CapitalDelta]1324)[[i]][[All,1]], {i, Length@ansatzMT\[CapitalDelta]1324}]], a_ /;Variables[a]==={}] //DeleteDuplicates; 

ansatzMT\[CapitalDelta]1325=GetMomentumTwistorExpression[coeffansatz, PSanalytic] /.\[CapitalDelta]1325sub;
listofPossibleFactors\[CapitalDelta]1325=DeleteCases[Flatten[Table[(FactorList[#]&/@ansatzMT\[CapitalDelta]1325)[[i]][[All,1]], {i, Length@ansatzMT\[CapitalDelta]1325}]], a_ /;Variables[a]==={}] //DeleteDuplicates; 

ansatzMT\[CapitalDelta]1524=GetMomentumTwistorExpression[coeffansatz, PSanalytic] /.\[CapitalDelta]1524sub;
listofPossibleFactors\[CapitalDelta]1524=DeleteCases[Flatten[Table[(FactorList[#]&/@ansatzMT\[CapitalDelta]1524)[[i]][[All,1]], {i, Length@ansatzMT\[CapitalDelta]1524}]], a_ /;Variables[a]==={}] //DeleteDuplicates; 

ansatzMT\[CapitalDelta]1523=GetMomentumTwistorExpression[coeffansatz, PSanalytic] /.\[CapitalDelta]1523sub;
listofPossibleFactors\[CapitalDelta]1523=DeleteCases[Flatten[Table[(FactorList[#]&/@ansatzMT\[CapitalDelta]1523)[[i]][[All,1]], {i, Length@ansatzMT\[CapitalDelta]1523}]], a_ /;Variables[a]==={}] //DeleteDuplicates; 

ansatzMT\[CapitalDelta]1423=GetMomentumTwistorExpression[coeffansatz, PSanalytic] /.\[CapitalDelta]1423sub;
listofPossibleFactors\[CapitalDelta]1423=DeleteCases[Flatten[Table[(FactorList[#]&/@ansatzMT\[CapitalDelta]1423)[[i]][[All,1]], {i, Length@ansatzMT\[CapitalDelta]1423}]], a_ /;Variables[a]==={}] //DeleteDuplicates; 


(* ::Subsubsection:: *)
(*Ansatzing the partial fractioning of coeffLogs13*)


(* ::Item:: *)
(*Partial fraction with respect to the 2 different poles*)


Factor/@(Apart[coeffLogs13, ex[4]]);


Factor/@(Apart[coeffLogs13, ex[4]] /.\[CapitalDelta]1324sub);
Factor/@(Apart[coeffLogs13, ex[4]] /.\[CapitalDelta]1325sub)


(* ::Item:: *)
(*Extracting the possible forms of the deepest poles of the coefficient*)


extractFromProjection[(coeffLogs13), coeffansatz, \[CapitalDelta]1324,2];
extractFromProjection[(coeffLogs13), coeffansatz, \[CapitalDelta]1325,2];


(* ::Item:: *)
(*Performing all the possible subtractions of the coefficients ansatze. Does the order in which we subtract the deepest pole matter? The loop is  faster  using coeffsFromPoles instead of extractFromProjection, but it's too slow in any case*)


starttime=AbsoluteTime[];

extr2s13delta1=coeffsFromPoles[(coeffLogs13), listofPossibleFactors\[CapitalDelta]1324, \[CapitalDelta]1324,2] /.\[Delta]->\[CapitalDelta]1324;
GetDegree[#]&/@Table[Together[(coeffLogs13-coeff/(\[CapitalDelta]1324^2) /.sliceRules)], {coeff,extr2s13delta1}] /.deg[i_,j_]:>i+j //Position[#, Min[#]]&;
extr2s13delta1= extr2s13delta1[[Flatten[%]]];
extr2s13delta2={};
extr1s13delta1={};
extr1s13delta2={};
Do[
   AppendTo[extr2s13delta2,coeffsFromPoles[((coeffLogs13)-term2s13delta1/\[CapitalDelta]1324^2), listofPossibleFactors\[CapitalDelta]1325, \[CapitalDelta]1325,2]/.\[Delta]->\[CapitalDelta]1325];
   extr2s13delta2= DeleteDuplicates[extr2s13delta2];
   Do[
      AppendTo[extr1s13delta1,coeffsFromPoles[(coeffLogs13- term2s13delta2/\[CapitalDelta]1325^2), listofPossibleFactors\[CapitalDelta]1324, \[CapitalDelta]1324,1, "DeeperPole"->term2s13delta1]/.\[Delta]->\[CapitalDelta]1324];
      extr1s13delta1= DeleteDuplicates[extr1s13delta1];
      Do[
         AppendTo[extr1s13delta2,coeffsFromPoles[(coeffLogs13-term2s13delta1/\[CapitalDelta]1324^2-term1s13delta1/\[CapitalDelta]1324), listofPossibleFactors\[CapitalDelta]1325, \[CapitalDelta]1325,1, "DeeperPole"->term2s13delta2]/.\[Delta]->\[CapitalDelta]1325];
         extr1s13delta2= DeleteDuplicates[extr1s13delta2];
         
         ,{term1s13delta1, Flatten[extr1s13delta1]}];
         
       ,{term2s13delta2, Flatten[extr2s13delta2]}];     
             
   ,{term2s13delta1,extr2s13delta1}];
   
timeExp= AbsoluteTime[]-starttime


(* ::Item:: *)
(*Subtract the coefficients in a specific order. Every time we select the coefficient which, when subtracted, lowers the degree the most. We get matching factors frmo different bubble coefficients*)


starttime=AbsoluteTime[];
extr2s13delta1= extractFromProjection[coeffLogs13, coeffansatz, \[CapitalDelta]1324,2] /.\[Delta]->\[CapitalDelta]1324;
GetDegree[#]&/@Table[Together[(coeffLogs13-coeff/(\[CapitalDelta]1324^2) /.sliceRules)], {coeff,extr2s13delta1}] /.deg[i_,j_]:>i+j //Position[#, Min[#]]&;
min2s13delta1= extr2s13delta1[[%[[1,1]]]];
extr2s13delta2=extractFromProjection[coeffLogs13-min2s13delta1/(\[CapitalDelta]1324^2), coeffansatz, \[CapitalDelta]1325,2] /.\[Delta]->\[CapitalDelta]1325;
GetDegree[#]&/@Table[Together[(coeffLogs13-min2s13delta1/(\[CapitalDelta]1324^2)-coeff/(\[CapitalDelta]1325^2) /.sliceRules)], {coeff,extr2s13delta2}] /.deg[i_,j_]:>i+j //Position[#, Min[#]]&;
min2s13delta2= extr2s13delta2[[%[[1,1]]]];
extr1s13delta1=coeffsFromPoles[coeffLogs13-min2s13delta2/(\[CapitalDelta]1325^2), listofPossibleFactors\[CapitalDelta]1324, \[CapitalDelta]1324,1, "DeeperPole"->min2s13delta1] /.\[Delta]->\[CapitalDelta]1324;
GetDegree[#]&/@Table[Together[(coeffLogs13-min2s13delta1/(\[CapitalDelta]1324^2)-min2s13delta2/(\[CapitalDelta]1325^2)-coeff/(\[CapitalDelta]1324) /.sliceRules)], {coeff,extr1s13delta1}] /.deg[i_,j_]:>i+j //Position[#, Min[#]]&;
min1s13delta1= extr1s13delta1[[%[[1,1]]]];
extr1s13delta2=coeffsFromPoles[coeffLogs13-min2s13delta1/(\[CapitalDelta]1324^2), listofPossibleFactors\[CapitalDelta]1325, \[CapitalDelta]1325,1, "DeeperPole"->min2s13delta2] /.\[Delta]->\[CapitalDelta]1325;
GetDegree[#]&/@Table[Together[(coeffLogs13-min2s13delta1/(\[CapitalDelta]1324^2)-min2s13delta2/(\[CapitalDelta]1325^2)-min1s13delta1/(\[CapitalDelta]1324)-coeff/(\[CapitalDelta]1325) /.sliceRules)], {coeff,extr1s13delta2}] /.deg[i_,j_]:>i+j //Position[#, Min[#]]&;
min1s13delta2= extr1s13delta2[[%[[1,1]]]];
time=AbsoluteTime[]-starttime


resCoeffLogs13=(coeffLogs13- min2s13delta1/(\[CapitalDelta]1324^2)- min2s13delta2/(\[CapitalDelta]1325^2)- min1s13delta1/(\[CapitalDelta]1324)- min1s13delta2/(\[CapitalDelta]1325));
expTrial=Together[resCoeffLogs13 /.sliceRules];
GetDegree[expTrial]


(* ::Subsubsection:: *)
(*Looking at the coefficients of the other bubbles*)


Factor/@(Apart[coeffLogs23, ex[4]] /.\[CapitalDelta]1523sub);
Factor/@(Apart[coeffLogs23, ex[4]] /.\[CapitalDelta]1423sub);


Factor/@(Apart[coeffLogs15, ex[4]] /.\[CapitalDelta]1523sub);


(* ::InheritFromParent:: *)
(**)


Factor/@(Apart[coeffLogs14, ex[4]] /.\[CapitalDelta]1423sub);


Factor/@(Apart[coeffLogs25, ex[4]]/.\[CapitalDelta]1325sub );


extr2s24delta1=extractFromProjection[coeffLogs24, coeffansatz, \[CapitalDelta]1324, 2]/.\[Delta]->\[CapitalDelta]1324;
GetDegree[#]&/@Table[Together[(coeffLogs24-coeff/(\[CapitalDelta]1324^2) /.sliceRules)], {coeff,extr2s24delta1}] /.deg[i_,j_]:>i+j //Position[#, Min[#]]&;
min2s24delta1= extr2s24delta1[[%[[1,1]]]];
extr1s24delta1=coeffsFromPoles[coeffLogs24, listofPossibleFactors\[CapitalDelta]1324, \[CapitalDelta]1324,1, "DeeperPole"->min2s24delta1] /.\[Delta]->\[CapitalDelta]1324;
GetDegree[#]&/@Table[Together[(coeffLogs24-min2s24delta1/(\[CapitalDelta]1324^2)-coeff/(\[CapitalDelta]1324) /.sliceRules)], {coeff,extr1s24delta1}] /.deg[i_,j_]:>i+j //Position[#, Min[#]]&;
min1s24delta1= extr1s24delta1[[%[[1,1]]]];
extr1s24delta5=coeffsFromPoles[coeffLogs24-min2s24delta1/(\[CapitalDelta]1324^2), listofPossibleFactors\[CapitalDelta]1524, \[CapitalDelta]1524,1] /.\[Delta]->\[CapitalDelta]1524;
GetDegree[#]&/@Table[Together[(coeffLogs24-min2s24delta1/(\[CapitalDelta]1324^2)-min1s24delta1/(\[CapitalDelta]1324)-coeff/(\[CapitalDelta]1524) /.sliceRules)], {coeff,extr1s24delta5}] /.deg[i_,j_]:>i+j //Position[#, Min[#]]&;
min1s24delta5= extr1s24delta5[[%[[1,1]]]];


extr1s24delta5=coeffsFromPoles[coeffLogs24+min2s13delta1/(\[CapitalDelta]1324^2), listofPossibleFactors\[CapitalDelta]1524, \[CapitalDelta]1524,1] /.\[Delta]->\[CapitalDelta]1524;
GetDegree[#]&/@Table[Together[(coeffLogs24+min2s13delta1/(\[CapitalDelta]1324^2)+min1s13delta1/(\[CapitalDelta]1324)-coeff/(\[CapitalDelta]1524) /.sliceRules)], {coeff,extr1s24delta5}] /.deg[i_,j_]:>i+j //Position[#, Min[#]]&;
min1s24delta5= extr1s24delta5[[%[[1,1]]]];


resCoeffLogs24=(coeffLogs24+ min2s13delta1/(\[CapitalDelta]1324^2)+min1s13delta1/(\[CapitalDelta]1324)- min1s24delta5/(\[CapitalDelta]1524));
GetDegree[Together[resCoeffLogs24 /.sliceRules]]


min2s24delta1 //Factor
min2s13delta1 //Factor


min1s24delta1 //Factor
min1s13delta1 //Factor


(* ::Item:: *)
(*The 1/\[Delta] terms obtained in this way coincide and allow to construct a coefficient for Log(s13/s24)*)


min2s13delta1 //Factor
min2s24delta1 //Factor


extr2s15delta1=extractFromProjection[coeffLogs15, coeffansatz, \[CapitalDelta]1523, 2]/.\[Delta]->\[CapitalDelta]1523;
GetDegree[#]&/@Table[Together[(coeffLogs15-coeff/(\[CapitalDelta]1523^2) /.sliceRules)], {coeff,extr2s15delta1}] /.deg[i_,j_]:>i+j //Position[#, Min[#]]&;
min2s15delta1= extr2s15delta1[[%[[1,1]]]];
extr1s15delta1=coeffsFromPoles[coeffLogs15, listofPossibleFactors\[CapitalDelta]1523, \[CapitalDelta]1523,1, "DeeperPole"->min2s15delta1] /.\[Delta]->\[CapitalDelta]1523;
GetDegree[#]&/@Table[Together[(coeffLogs15-min2s15delta1/(\[CapitalDelta]1523^2)-coeff/(\[CapitalDelta]1523) /.sliceRules)], {coeff,extr1s15delta1}] /.deg[i_,j_]:>i+j //Position[#, Min[#]]&;
min1s15delta1= extr1s15delta1[[%[[1,1]]]];
extr1s15delta5=coeffsFromPoles[coeffLogs15-min2s15delta1/(\[CapitalDelta]1523^2)-min1s15delta1/(\[CapitalDelta]1523), listofPossibleFactors\[CapitalDelta]1524, \[CapitalDelta]1524,1] /.\[Delta]->\[CapitalDelta]1524;
GetDegree[#]&/@Table[Together[(coeffLogs15-min2s15delta1/(\[CapitalDelta]1523^2)-min1s15delta1/(\[CapitalDelta]1523)-coeff/(\[CapitalDelta]1524) /.sliceRules)], {coeff,extr1s15delta5}] /.deg[i_,j_]:>i+j //Position[#, Min[#]]&;
min1s15delta5= extr1s15delta5[[%[[1,1]]]];


extr2s15delta1=coeffsFromPoles[coeffLogs15, listofPossibleFactors\[CapitalDelta]1523, \[CapitalDelta]1523, 2]/.\[Delta]->\[CapitalDelta]1523;
GetDegree[#]&/@Table[Together[(coeffLogs15-coeff/(\[CapitalDelta]1523^2) /.sliceRules)], {coeff,extr2s15delta1}] /.deg[i_,j_]:>i+j //Position[#, Min[#]]&;
min2s15delta1= extr2s15delta1[[%[[1,1]]]];
extr1s15delta1=coeffsFromPoles[coeffLogs15, listofPossibleFactors\[CapitalDelta]1523, \[CapitalDelta]1523,1, "DeeperPole"->min2s15delta1] /.\[Delta]->\[CapitalDelta]1523;
GetDegree[#]&/@Table[Together[(coeffLogs15-min2s15delta1/(\[CapitalDelta]1523^2)-coeff/(\[CapitalDelta]1523) /.sliceRules)], {coeff,extr1s15delta1}] /.deg[i_,j_]:>i+j //Position[#, Min[#]]&;
min1s15delta1= extr1s15delta1[[%[[1,1]]]];


resCoeffLogs15=(coeffLogs15-min2s15delta1/(\[CapitalDelta]1523^2)-min1s15delta1/(\[CapitalDelta]1523)+min1s24delta5/(\[CapitalDelta]1524));
GetDegree[Together[resCoeffLogs15 /.sliceRules]]


min1s24delta5 //Factor
min1s15delta5 //Factor


extr2s23delta1=extractFromProjection[coeffLogs23, coeffansatz, \[CapitalDelta]1523, 2]/.\[Delta]->\[CapitalDelta]1523;
GetDegree[#]&/@Table[Together[(coeffLogs23-coeff/(\[CapitalDelta]1523^2) /.sliceRules)], {coeff,extr2s23delta1}] /.deg[i_,j_]:>i+j //Position[#, Min[#]]&;
min2s23delta1= extr2s23delta1[[%[[1,1]]]];
extr2s23delta5=extractFromProjection[coeffLogs23, coeffansatz, \[CapitalDelta]1423, 2]/.\[Delta]->\[CapitalDelta]1423;
GetDegree[#]&/@Table[Together[(coeffLogs23-min2s23delta1/(\[CapitalDelta]1523^2) -coeff/(\[CapitalDelta]1423^2) /.sliceRules)], {coeff,extr2s23delta5}] /.deg[i_,j_]:>i+j //Position[#, Min[#]]&;
min2s23delta5= extr2s23delta5[[%[[1,1]]]];
extr1s23delta1=coeffsFromPoles[coeffLogs23, listofPossibleFactors\[CapitalDelta]1523, \[CapitalDelta]1523,1, "DeeperPole"->min2s23delta1] /.\[Delta]->\[CapitalDelta]1523;
GetDegree[#]&/@Table[Together[(coeffLogs23-min2s23delta1/(\[CapitalDelta]1523^2) -min2s23delta5/(\[CapitalDelta]1423^2) -coeff/(\[CapitalDelta]1523) /.sliceRules)], {coeff,extr1s23delta1}] /.deg[i_,j_]:>i+j //Position[#, Min[#]]&;
min1s23delta1= extr1s23delta1[[%[[1,1]]]];
extr1s23delta5=coeffsFromPoles[coeffLogs23-min2s23delta1/(\[CapitalDelta]1523^2)-min2s23delta5/(\[CapitalDelta]1423^2)-min1s23delta1/(\[CapitalDelta]1523), listofPossibleFactors\[CapitalDelta]1423, \[CapitalDelta]1423,1] /.\[Delta]->\[CapitalDelta]1423;
GetDegree[#]&/@Table[Together[(coeffLogs23-min2s23delta1/(\[CapitalDelta]1523^2)-min2s23delta5/(\[CapitalDelta]1423^2)-min1s23delta1/(\[CapitalDelta]1523)-coeff/(\[CapitalDelta]1423) /.sliceRules)], {coeff,extr1s23delta5}] /.deg[i_,j_]:>i+j //Position[#, Min[#]]&;
min1s23delta5= extr1s23delta5[[%[[1,1]]]];


extr2s23delta5=coeffsFromPoles[coeffLogs23, listofPossibleFactors\[CapitalDelta]1423, \[CapitalDelta]1423, 2]/.\[Delta]->\[CapitalDelta]1423;
GetDegree[#]&/@Table[Together[(coeffLogs23+min2s15delta1/(\[CapitalDelta]1523^2) -coeff/(\[CapitalDelta]1423^2) /.sliceRules)], {coeff,extr2s23delta5}] /.deg[i_,j_]:>i+j //Position[#, Min[#]]&;
min2s23delta5= extr2s23delta5[[%[[1,1]]]];

extr1s23delta5=coeffsFromPoles[coeffLogs23+min2s15delta1/(\[CapitalDelta]1523^2)-min2s23delta5/(\[CapitalDelta]1423^2)+min1s15delta1/(\[CapitalDelta]1523), listofPossibleFactors\[CapitalDelta]1423, \[CapitalDelta]1423,1] /.\[Delta]->\[CapitalDelta]1423;
GetDegree[#]&/@Table[Together[(coeffLogs23+min2s15delta1/(\[CapitalDelta]1523^2)-min2s23delta5/(\[CapitalDelta]1423^2)+min1s15delta1/(\[CapitalDelta]1523)-coeff/(\[CapitalDelta]1423) /.sliceRules)], {coeff,extr1s23delta5}] /.deg[i_,j_]:>i+j //Position[#, Min[#]]&;
min1s23delta5= extr1s23delta5[[%[[1,1]]]];


resCoeffLogs23=(coeffLogs23+min2s15delta1/(\[CapitalDelta]1523^2)- min2s23delta5/(\[CapitalDelta]1423^2)+ min1s15delta1/(\[CapitalDelta]1523)- min1s23delta5/(\[CapitalDelta]1423));
GetDegree[Together[resCoeffLogs23 /.sliceRules]]


min2s23delta1 //Factor
min2s15delta1 //Factor


min1s23delta1 //Factor
min1s15delta1 //Factor


min1s23delta5 //Factor
min1s14\[CapitalDelta]1423 //Factor


min2s15delta1 //Factor
min2s23delta1 //Factor


extr2s14\[CapitalDelta]1423=extractFromProjection[coeffLogs14, coeffansatz, \[CapitalDelta]1423, 2]/.\[Delta]->\[CapitalDelta]1423;
GetDegree[#]&/@Table[Together[(coeffLogs14-coeff/(\[CapitalDelta]1423^2) /.sliceRules)], {coeff,extr2s14\[CapitalDelta]1423}] /.deg[i_,j_]:>i+j //Position[#, Min[#]]&;
min2s14\[CapitalDelta]1423= extr2s14\[CapitalDelta]1423[[%[[1,1]]]];

extr1s14\[CapitalDelta]1423=coeffsFromPoles[coeffLogs14, listofPossibleFactors\[CapitalDelta]1423, \[CapitalDelta]1423,1, "DeeperPole"->min2s14\[CapitalDelta]1423] /.\[Delta]->\[CapitalDelta]1423;
GetDegree[#]&/@Table[Together[(coeffLogs14-min2s14\[CapitalDelta]1423/(\[CapitalDelta]1423^2) -coeff/(\[CapitalDelta]1423) /.sliceRules)], {coeff,extr1s14\[CapitalDelta]1423}] /.deg[i_,j_]:>i+j //Position[#, Min[#]]&;
min1s14\[CapitalDelta]1423= extr1s14\[CapitalDelta]1423[[%[[1,1]]]];


resCoeffLogs14=(coeffLogs14+min2s23delta5/(\[CapitalDelta]1423^2)+min1s23delta5/(\[CapitalDelta]1423));
GetDegree[Together[resCoeffLogs14 /.sliceRules]]


min2s14\[CapitalDelta]1423 //Factor
min2s23delta5 //Factor


min1s14\[CapitalDelta]1423 //Factor
min1s23delta5 //Factor


extr2s25\[CapitalDelta]1325=extractFromProjection[coeffLogs25, coeffansatz, \[CapitalDelta]1325, 2]/.\[Delta]->\[CapitalDelta]1325;
GetDegree[#]&/@Table[Together[(coeffLogs25-coeff/(\[CapitalDelta]1325^2) /.sliceRules)], {coeff,extr2s25\[CapitalDelta]1325}] /.deg[i_,j_]:>i+j //Position[#, Min[#]]&;
min2s25\[CapitalDelta]1325= extr2s25\[CapitalDelta]1325[[%[[1,1]]]];

extr1s25\[CapitalDelta]1325=coeffsFromPoles[coeffLogs25, listofPossibleFactors\[CapitalDelta]1325, \[CapitalDelta]1325,1, "DeeperPole"->min2s25\[CapitalDelta]1325] /.\[Delta]->\[CapitalDelta]1325;
GetDegree[#]&/@Table[Together[(coeffLogs25-min2s25\[CapitalDelta]1325/(\[CapitalDelta]1325^2) -coeff/(\[CapitalDelta]1325) /.sliceRules)], {coeff,extr1s25\[CapitalDelta]1325}] /.deg[i_,j_]:>i+j //Position[#, Min[#]]&;
min1s25\[CapitalDelta]1325= extr1s25\[CapitalDelta]1325[[%[[1,1]]]];


min2s25\[CapitalDelta]1325 //Factor
min2s13delta2 //Factor


min1s25\[CapitalDelta]1325 //Factor
min1s13delta2 //Factor


resCoeffLogs25= (coeffLogs25+ min2s13delta2/(\[CapitalDelta]1325^2)+ min1s13delta2/(\[CapitalDelta]1325));
GetDegree[Together[resCoeffLogs25 /.sliceRules]]
GetDegree[Together[coeffLogs25 /.sliceRules]]


(* ::Item:: *)
(*We see that the reduced coefficients have lower degree than the original ones*)


cLogList={coeffLogs13,coeffLogs14,coeffLogs15,coeffLogs23,coeffLogs24,coeffLogs25};
nrm=MatchCoefficientFactors[(cLogList /.sliceRules), coeffFactorsGuess] /.tiz[i__]:>0;
GetDegree[Together[#/.sliceRules]]&/@(cLogList/nrm)


cResList={resCoeffLogs13,resCoeffLogs14,resCoeffLogs15,resCoeffLogs23,resCoeffLogs24,resCoeffLogs25};
nrmRes=MatchCoefficientFactors[(cResList /.sliceRules), coeffFactorsGuess] /.tiz[i__]:>0;
GetDegree[Together[#/.sliceRules]]&/@(cResList/nrmRes)


(* ::Subsubsection:: *)
(*Performing the linear fit after the subtraction of the guessed coefficients*)


(* ::Item:: *)
(*First we perform a straightforward linear fit to reconstruct the Lhtats coeffs directly*)


vrs=ex[#]&/@Range[5];


FFNewGraph[fittingGraphOld, input, vrs]
ansatz=a1523*(Log[s15]-Log[s13])+a2314*(Log[s23]-Log[s14])+a1324*(Log[s13]-Log[s24])+a1325*(Log[s13]-Log[s25])+a1425*(Log[s14]-Log[s25]);
rhs= coeffLogs13*Log[s13]+coeffLogs14*Log[s14]+coeffLogs15*Log[s15]+coeffLogs24*Log[s24]+coeffLogs23*Log[s23]+coeffLogs25*Log[s25];
FFAlgLinearFit[fittingGraphOld, fitting, {input}, vrs, ansatz, rhs, {Log[s13],Log[s14],Log[s15],Log[s23],Log[s24],Log[s25]}, {a1523,a2314,a1325,a1324, a1425} ]
FFGraphOutput[fittingGraphOld, fitting]
learninfo= FFDenseSolverLearn[fittingGraphOld, vrs]


reconstructedOld=FFReconstructFunction[fittingGraphOld, vrs];
normOld=(MatchCoefficientFactors[(reconstructedOld /.sliceRules), coeffFactorsGuess] /.tiz[i__]:>0);


GetDegree[Together[#/.sliceRules]]&/@(reconstructedOld/normOld)


(* ::Item:: *)
(*Compare the result above with the fit performed on the subtracted expressions. The degree is lowered.*)


c13= min2s13delta1/(\[CapitalDelta]1324^2)+min2s13delta2/(\[CapitalDelta]1325^2)+min1s13delta1/(\[CapitalDelta]1324)+min1s13delta2/(\[CapitalDelta]1325)+resCoeffLogs13;
c15= min2s15delta1/(\[CapitalDelta]1523^2)+min1s15delta1/(\[CapitalDelta]1523)-min1s24delta5/(\[CapitalDelta]1524)+resCoeffLogs15;
c14= -min2s23delta5/(\[CapitalDelta]1423^2)-min1s23delta5/(\[CapitalDelta]1423)+resCoeffLogs14;
c23= -min2s15delta1/(\[CapitalDelta]1523^2)+min2s23delta5/(\[CapitalDelta]1423^2)- min1s15delta1/(\[CapitalDelta]1523)+ min1s23delta5/(\[CapitalDelta]1423)+ resCoeffLogs23;
c24=  -min2s13delta1/(\[CapitalDelta]1324^2)-min1s13delta1/(\[CapitalDelta]1324)+min1s24delta5/(\[CapitalDelta]1524)+resCoeffLogs24;
c25= -min2s13delta2/(\[CapitalDelta]1325^2)-min1s13delta2/(\[CapitalDelta]1325)+resCoeffLogs25; 


FFNewGraph[fittingGraph, input, vrs]
ansatz=a1523*(Log[s15]-Log[s23])+a2314*(Log[s23]-Log[s14])+a2413*(Log[s24]-Log[s13])+a2513*(Log[s25]-Log[s13])+a1524*(Log[s15]-Log[s24]);
rhs= resCoeffLogs13*Log[s13]+resCoeffLogs14*Log[s14]+resCoeffLogs15*Log[s15]+resCoeffLogs24*Log[s24]+resCoeffLogs23*Log[s23]+resCoeffLogs25*Log[s25];
FFAlgLinearFit[fittingGraph, fitting, {input}, vrs, ansatz, rhs, {Log[s13],Log[s14],Log[s15],Log[s23],Log[s24],Log[s25]}, {a1523,a2314,a2513,a2413, a1524} ]
FFGraphOutput[fittingGraph, fitting]
learninfo= FFDenseSolverLearn[fittingGraph, vrs]


reconstructed=FFReconstructFunction[fittingGraph, vrs];
norm=(MatchCoefficientFactors[(reconstructed /.sliceRules), coeffFactorsGuess] /.tiz[i__]:>0);


GetDegree[Together[#/.sliceRules]]&/@(reconstructed/norm)
