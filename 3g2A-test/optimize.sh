
#
form optimize_+mm+m.frm

sed -i "s/\ \([0-9]\+\)/ static_cast<T>(\1.)/g" optimize_+mm+m.c

sed -i "s/=\([0-9]\+\)\ /=static_cast<T>(\1.) /g" optimize_+mm+m.c
sed -i "s/(\([0-9]\+\)\ /(static_cast<T>(\1.) /g" optimize_+mm+m.c
sed -i "s/{\([0-9]\+\)\ /{static_cast<T>(\1.) /g" optimize_+mm+m.c

sed -i "s/=\([0-9]\+\)\*/=static_cast<T>(\1.)*/g" optimize_+mm+m.c
sed -i "s/(\([0-9]\+\)\*/(static_cast<T>(\1.)*/g" optimize_+mm+m.c
sed -i "s/{\([0-9]\+\)\*/{static_cast<T>(\1.)*/g" optimize_+mm+m.c

sed -i "s/,\([0-9]\+\))/,static_cast<T>(\1.))/g" optimize_+mm+m.c

sed -i "s/1\//static_cast<T>(1.)\//g" optimize_+mm+m.c

sed -i "s/pow(\(\w\+\),-1)/static_cast<T>(1.)\/\1/g" optimize_+mm+m.c

`for n in $(seq 1 99); do
    m=$((n - 1))
    sed -ir "s/\[$n\]/[$m]/g" optimize_+mm+m.c
done`




#form optimize_m++++.frm
#
#sed -i "s/\ \([0-9]\+\)/ static_cast<T>(\1.)/g" optimize_m++++.c
#
#sed -i "s/=\([0-9]\+\)\ /=static_cast<T>(\1.) /g" optimize_m++++.c
#sed -i "s/(\([0-9]\+\)\ /(static_cast<T>(\1.) /g" optimize_m++++.c
#sed -i "s/{\([0-9]\+\)\ /{static_cast<T>(\1.) /g" optimize_m++++.c
#
#sed -i "s/=\([0-9]\+\)\*/=static_cast<T>(\1.)*/g" optimize_m++++.c
#sed -i "s/(\([0-9]\+\)\*/(static_cast<T>(\1.)*/g" optimize_m++++.c
#sed -i "s/{\([0-9]\+\)\*/{static_cast<T>(\1.)*/g" optimize_m++++.c
#
#sed -i "s/,\([0-9]\+\))/,static_cast<T>(\1.))/g" optimize_m++++.c
#
#sed -i "s/1\//static_cast<T>(1.)\//g" optimize_m++++.c
#
#sed -i "s/pow(\(\w\+\),-1)/static_cast<T>(1.)\/\1/g" optimize_m++++.c
#
#`for n in $(seq 1 99); do
#    m=$((n - 1))
#    sed -ir "s/\[$n\]/[$m]/g" optimize_m++++.c
#done`
#

form optimize_pppmm.frm

sed -i "s/\ \([0-9]\+\)/ static_cast<T>(\1.)/g" optimize_pppmm.c

sed -i "s/=\([0-9]\+\)\ /=static_cast<T>(\1.) /g" optimize_pppmm.c
sed -i "s/(\([0-9]\+\)\ /(static_cast<T>(\1.) /g" optimize_pppmm.c
sed -i "s/{\([0-9]\+\)\ /{static_cast<T>(\1.) /g" optimize_pppmm.c

sed -i "s/=\([0-9]\+\)\*/=static_cast<T>(\1.)*/g" optimize_pppmm.c
sed -i "s/(\([0-9]\+\)\*/(static_cast<T>(\1.)*/g" optimize_pppmm.c
sed -i "s/{\([0-9]\+\)\*/{static_cast<T>(\1.)*/g" optimize_pppmm.c

sed -i "s/,\([0-9]\+\))/,static_cast<T>(\1.))/g" optimize_pppmm.c

sed -i "s/1\//static_cast<T>(1.)\//g" optimize_pppmm.c

sed -i "s/pow(\(\w\+\),-1)/static_cast<T>(1.)\/\1/g" optimize_pppmm.c

`for n in $(seq 1 99); do
    m=$((n - 1))
    sed -ir "s/\[$n\]/[$m]/g" optimize_pppmm.c
done`
#



