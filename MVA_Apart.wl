(* ::Package:: *)

Needs["MultivariateApart`"]


<<FiniteFlow`
Get["git/gitlab/myfiniteflowexamples/TwoLoopTools/BCFWreconstructionTools/ReconstructFunctionApart.wl"];
Get["git/gitlab/myfiniteflowexamples/TwoLoopTools/BCFWreconstructionTools/BCFWReconstructFunction.wl"];


RemoveOverallConstants[list_List]:=Which[NumberQ[#],1, NumberQ[#[[1]]], #/#[[1]], True, #]&/@list


(*expr=Get["git/gitlab/paper_1l2t3g/amplitudes/Ncpm1T23451/GlobalMap_1L_ttggg_T23451__Oqt2qt1_Ncp-1_dsm2p1_+++-+_PStt3g33_mren.m"];*)
expr=Get["git/gitlab/paper_1l2t3g/amplitudes/NcT23451/GlobalMap_1L_ttggg_T23451__Oqt2qt1_Ncp1_dsm2p0_+++-+_PStt3g33_mren.m"];


num=Numerator[Together[expr[[1,7]]]]  /.eps->1/10;
den=Denominator[Together[expr[[1,7]]]] /.eps->1/10;
num=num/(s34 (t45+t23 t45+t12 t51-t45 t51-t12 x5123+t12^2 x5123-t12 t45 x5123) (-t12 t45-t23 t45+t45^2-t12 t51+t45 t51+t12 x5123-t12^2 x5123+t12 t45 x5123));


vrs=Variables[num]


FFNewGraph[graph, in, vrs];
FFAlgRatFunEval[graph, evalNode, {in}, vrs,{num}];
FFGraphOutput[graph, evalNode];


(* ::Item:: *)
(*Choose two variables to slice in *)


bivrs = {t51,x5123};
othervrs = Complement[vrs,bivrs];
bislice = Thread[othervrs->RandomInteger[{10^4,10^10},Length[othervrs]]]


numsliced = Collect[num/.bislice,bivrs,FFRatMod[#,FFPrimeNo[0]]&];


(* ::Item:: *)
(*Separate dens which depend on bivars and slice them*)


Dens=FactorList[den];
densSliced=Expand[Dens/.bislice , Modulus->FFPrimeNo[0]];
(*densSliced=Collect[dens/.bislice,bivrs,FFRatMod[#,FFPrimeNo[0]]&];*)
Position[densSliced, #, 2]&/@Select[Transpose[densSliced][[1]], NumericQ] /.{a__, 2}:>Nothing;
constTrms=Transpose[ArrayReshape[%, {Dimensions[%][[1]]*Dimensions[%][[2]], Dimensions[%][[3]]}]][[1]];
shortDens=Table[Dens[[i]] /.{a_, b_}:>a^b, {i, Complement[Range[Length@Dens],constTrms]}]
shortDensSliced=Table[densSliced[[i]] /.{a_, b_}:>a^b, {i, Complement[Range[Length@Dens],constTrms]}]
indepDens=Times@@Table[Dens[[i]] /.{a_, b_}:>a^b, {i, constTrms}]


Clear[fn];
fn=(numsliced/(Times@@shortDensSliced));


mvaoutput=MultivariateApart[fn];
mvaoutput- fn //Simplify


Clear[originaldenFacs]
Clear[originaldenFacsSliced]
FactorList/@shortDens;
originaldenFacs=Transpose[ArrayReshape[%, {Dimensions[%][[1]]*Dimensions[%][[2]], Dimensions[%][[3]]}]][[1]];
originaldenFacs=DeleteDuplicates[originaldenFacs];
originaldenFacs=Select[originaldenFacs, Variables[#]!={}&]


FactorList/@shortDensSliced;
originaldenFacsSliced=DeleteDuplicates[Transpose[ArrayReshape[%, {Dimensions[%][[1]]*Dimensions[%][[2]], Dimensions[%][[3]]}]][[1]]];
originaldenFacsSliced=Select[originaldenFacsSliced, Variables[#]!={}&];


Length@originaldenFacs
Length@originaldenFacsSliced


dSymbols=h[#]&/@Range[Length@originaldenFacsSliced]


mvaoutputList=List@@Expand[mvaoutput];
numList=Numerator/@%;
dnList=Denominator/@%%;
denFacs=FactorList/@dnList;
pows=Transpose[#][[2]]&/@denFacs;


(* ::Item:: *)
(*Match the slices with the denominator factors*)


Clear[matchedDens]
Clear[matchedFacs]
matchedDens={};
Do[
   toMatch=Transpose[d][[1]];
   matchedFacs={};
   Do[
      pos=Position[originaldenFacsSliced, trm,1];
      If[pos!={},
         AppendTo[matchedFacs, dSymbols[[pos[[1,1]]]]]; 
         ,
         AppendTo[matchedFacs, trm];
         ];
      , {trm, toMatch}];
      AppendTo[matchedDens, matchedFacs];
   , {d, denFacs}];


Table[matchedDens[[i]]^pows[[i]], {i, Length@pows}];
numList/Times@@@%;
% /.h[i_]:>originaldenFacsSliced[[i]];
%-mvaoutputList


(mvaoutputList/.List->Plus)/mvaoutput //Simplify;
%===1


Table[matchedDens[[i]]^pows[[i]], {i, Length@pows}];
numList/Times@@@%;
Variables[Denominator/@%]
(%% /.h[i_]:>originaldenFacs[[i]] )*(Times@@shortDens);
Factor/@%;
ansatznum=RemoveOverallConstants[%];


(* ::Item:: *)
(*A longer ansatznum increases the evaluation time per point*)


Length@ansatznum


ansatznum=Simplify/@ansatznum;


Clear[ansatz]
ansatznumSliced=(Collect[#/.bislice,bivrs,FFRatMod[#,FFPrimeNo[0]]&])&/@ansatznum;
coeffs=c/@Range[Length@ansatznumSliced]


(* ::Item:: *)
(*Do the fit in the slice to identify the redundant terms*)


FFNewGraph[graphNum, in, bivrs];
FFAlgRatExprEval[graphNum, evalNode, {in}, bivrs,Join[ansatznumSliced,{numsliced}]];
FFGraphOutput[graphNum, evalNode];
FFNewGraph[fitGraphNum];
FFAlgSubgraphFit[fitGraphNum, fitNode, {}, graphNum, bivrs, coeffs];
FFGraphOutput[fitGraphNum, fitNode];
fitlearn=FFDenseSolverLearn[fitGraphNum, coeffs];
indepvars="IndepVars" /.fitlearn;


sortedvrs = Join[bivrs,othervrs];
acceptedterms = Variables[coeffs/.Thread[indepvars->0]] (* ARBITRARY CHOICE *)


ansatznum=ansatznum.coeffs;
ansatzterms = Simplify[Table[Coefficient[ansatznum,aa],{aa,acceptedterms}]];


(* ::Item:: *)
(*Do the fit in N-2 variables*)


FFNewGraph[graph2,in,sortedvrs];
FFAlgRatFunEval[graph2, evalNode,{in},sortedvrs,{num}];
FFAlgRatFunEval[graph2, AnsatzTerms,{in},sortedvrs,ansatzterms]//Print;
FFAlgChain[graph2,fiteqs,{AnsatzTerms,evalNode}]//Print;
FFGraphOutput[graph2,fiteqs];


qs=acceptedterms;

FFDeleteGraph[fitGraph];
FFNewGraph[fitGraph,in,othervrs]//Print;
FFAlgSubgraphFit[fitGraph,fitNode,{in},graph2,bivrs,qs]//Print;
FFGraphOutput[fitGraph, fitNode];


fitlearn=FFDenseSolverLearn[fitGraph, qs];


(* ::Item:: *)
(*Check the degree of the coefficients*)


{slicerules, coeffsSliced} = GetSlice[fitGraph,othervrs];
GetDegreeMax@coeffsSliced


<<FiniteFlow`
Get["/home/fsarandrea/git/gitlab/myfiniteflowexamples/InitTwoLoopToolsFF.m"];
<< "InitDiagramsFF.m"
process="6g";
psmode="PSanalytic";
Get["/home/fsarandrea/git/gitlab/myfiniteflowexamples/amplitudes/GlobalMapProcessSetup.m"];


fitrec=ReconstructFunctionFactors2[fitGraph, PSanalytic, PSanalytic, {ex[1]->1, ex[4]->1, ex[8]->1}, {"CoefficientAnsatz"->coeffansatz, "PrintDebugInfo"->2}]


fitsol = FFDenseSolverSol[fitrec,fitlearn];


res = (qs/.fitsol /. q[_]:>0) . (ansatzterms/den);


res = Total[Factor/@(List@@res)]
